﻿function ContactFormValidation() {
    var msg = contactUsValidation();
    if (msg.length > 0) {
        alert(msg);
    
    } else {
        $('#contactUsFrm').submit();
    //document.contactUsFrm.submit();
}
}
function contactUsValidation() {
    var msg = "";
    var frmVal = new FormValidator();
    var customerName = $('#CustomerName').val();// document.getElementById("CustomerName").value;
    msg += frmVal.isValidPersonName(customerName, "Customer Name");

    var CustomerContact = $('#CustomerContact').val();
    msg += frmVal.isValidMobile(CustomerContact, "Contact Number");

    var email = $('#CustomerEmail').val();
    msg += frmVal.isValidEmail(email, "Email");

    var CustomerMessage = $('#CustomerMessage').val();
    msg += frmVal.isValidDescription(CustomerMessage, "Customer Message");

    return msg;
}
function validateEducationalTourEnquiryForm() {
    var message = "";
    var frmVal = new FormValidator();
    var instituteName = $('#instituteName').val();
    var department = $('#department').val();
    var facultyName = $('#facultyName').val();
    var email = $('#email').val();
    var contactNumber = $('#contactNumber').val();
    var instituteAddress = $('#instituteAddress').val();
    var prefDestination = $('#prefDestination').val();
    var noOfStudents = $('#noOfStudents').val();
    var noOfTeachers = $('#noOfTeachers').val();
    var tentativeDate = $('#tentativeDate').val();

    message += frmVal.isValidOrganizationName(instituteName, "Institute Name");

    message += frmVal.isValidOrganizationName(department, "Department Name");

    message += frmVal.isValidPersonName(facultyName, "Faculty Name");

    message += frmVal.isValidMobile(contactNumber, "Contact Number");

    message += frmVal.isValidEmail(email, "Email");

    message += frmVal.isValidDescription(prefDestination, "preferred Destination");

    message += frmVal.isMandetoryNumeric(noOfStudents, "Number Of Students");

    message += frmVal.isMandetoryNumeric(noOfTeachers, "Number Of Teachers");

    message += frmVal.isValidDate(tentativeDate, "Tentative Date");

    return message;
}

function validateFarByCarEnquiryForm() {
    var message = "";
    var frmVal = new FormValidator();
    var customerName = $('#customerName').val();
    var customerEmail = $('#customerEmail').val();
    var customerContact = $('#customerContact').val();
    var NoOfTravellers = $('#noOfTravellers').val();
    var tentativeDate = $('#tentativeDate').val();
    //var customerEnquiry = $('#customerEnquiry').val();
    var destination = $('#preDestination').val();
    
    message += frmVal.isValidPersonName(customerName, "Customer Name");

    message += frmVal.isValidMobile(customerContact, "Contact Number");

    message += frmVal.isValidEmail(customerEmail, "Email");

    //message += frmVal.isMandetoryNumeric(NoOfTravellers, "Number Of Travellers");

    message += frmVal.isValidDescription(destination, "preferred Destination");

    //message += frmVal.isValidDescription(customerInquiry, "customerEnquiry");

    message += frmVal.isValidDate(tentativeDate, "Tentative Date");

    return message;
}

function ClearEduTourEnquiryForm() {
    $('#instituteName').val("");
    $('#department').val("");
    $('#facultyName').val("");
    $('#email').val("");
    $('#contactNumber').val("");
    $('#instituteAddress').val("");
    $('#prefDestination').val("");
    $('#noOfStudents').val("");
    $('#noOfTeachers').val("");
    $('#tentativeDate').val("");
}

function ClearFarByCarEnquiryForm() {
    $('#customerName').val("");
    $('#customerEmail').val("");
    $('#customerContact').val("");
    //$('#noOfTravellers').val("");
    $('#tentativeDate').val("");
    //$('#customerEnquiry').val("");
    $('#address').val("");
    $('#preDestination').val("");
    $('#Duration').val("");
    $('#adult').val("");
    $('#child').val("");
}
