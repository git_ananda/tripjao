/**
 * 
 */
function contactUsValidation() {
    var msg = "";
    var frmVal = new FormValidator();
    var customerName = $('#CustomerName').val();// document.getElementById("CustomerName").value;
    msg += frmVal.isValidPersonName(customerName, "CustomerName");

    var CustomerContact = $('#CustomerContact').val();
    msg += frmVal.isValidMobile(CustomerContact, "Contact no");

    var email = $('#CustomerEmail').val();
    msg += frmVal.isValidEmail(email, "Email");

    var CustomerMessage = $('#CustomerMessage').val();
    msg += frmVal.isValidDescription(CustomerMessage, "CustomerMessage");
    
    return msg;
}
function doValidation(){
	var msg = "";
	var frmVal = new FormValidator();
	
	var gender = document.getElementsByName("dd_gender")[0].value;
	msg += frmVal.isValidMandatoryComboValue(gender, "Gender");
	
	var memberName = document.getElementsByName("txt_membername")[0].value;
	msg += frmVal.isValidPersonName(memberName ,"Member name");
	
	var flatNumber = document.getElementsByName("txt_flatnumber")[0].value;
	flatNumber = flatNumber.trim();
	if(flatNumber.length <= 0){
		msg += "Invalid flat number <br>" ;
	}
	
	var dob = document.getElementsByName("txt_dob")[0].value;
	msg += frmVal.isValidDate(dob, "Date of Birth");
	
	var date = getDate(dob);
	var today = new Date();
	if( date >= today ){
		msg += "Future date is not allowed <br>";
	}
	
	var bloodGroup = document.getElementsByName("txt_bloodgroup")[0].value;
	msg += frmVal.isValidBloodGroup(bloodGroup, "Blood Group");
	
	var memberType = document.getElementsByName("dd_membertype")[0].value;
	msg += frmVal.isValidMandatoryComboValue(memberType, "Member Type");
	
	
	var memberClass = document.getElementsByName("dd_memberclass")[0].value;
	msg += frmVal.isValidMandatoryComboValue(memberClass, "Member Class");
	
	var mobileNo = document.getElementsByName("txt_primarymobileno")[0].value;
	msg += frmVal.isValidMobile(mobileNo, "Primary Mobile no");
	
	var telephoneNo = document.getElementsByName("txt_telephonenumber")[0].value;
	if(!frmVal.isEmpty(telephoneNo)){
		msg += frmVal.isValidTelephone(telephoneNo, "Telephone");
	}
	
	
	var secondMobileNo = document.getElementsByName("txt_secondarymobileno")[0].value;
	if(!frmVal.isEmpty(secondMobileNo)){
		msg += frmVal.isValidMobile(secondMobileNo, "Alternate mobile no");
	}
	
	var email = document.getElementsByName("txt_primaryemail")[0].value;
	msg += frmVal.isValidEmail(email, "Email");
	
	var secEmail = document.getElementsByName("txt_secondaryemail")[0].value;
	if(!frmVal.isEmpty(secEmail)){
		msg += frmVal.isValidEmail(secEmail, "Alternate Email");
	}
	
	var emrgMobileNo = document.getElementsByName("txt_emergmobilenumber")[0].value;
	if(!frmVal.isEmpty(emrgMobileNo)){
		msg += frmVal.isValidMobile(emrgMobileNo, "Emergency mobile no");
	}
	
	var emergTelePhoneNo = document.getElementsByName("txt_emergtelephonenumber")[0].value;
	if(!frmVal.isEmpty(emergTelePhoneNo)){
		msg += frmVal.isValidTelephone(emergTelePhoneNo, "Emergency telephone number");
	}
	return msg;
}

function getDate(strDate){
	var parms = strDate.split(/[\.\-\/]/);
	var yyyy = parseInt(parms[2]);
	var mm = parseInt(parms[1]);
	var dd = parseInt(parms[0]);
	var date = new Date(yyyy, mm - 1, dd, 0, 0, 0, 0);
	return date;
}