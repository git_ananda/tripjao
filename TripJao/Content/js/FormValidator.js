function FormValidator(){
	
	
	
	FormValidator.prototype.isValidLoginName = function(value, displayName) {

		if(this.isEmpty(value)){
            return (displayName + " should not be blank" + "\n");
		}
		
		if(!this.haveAllowedSpecialChars(value, "._")){
            return (displayName + " has only allowed special chars[._]" + "\n");
		}
		
		if(!this.isLengthBetweenRange(8, 15, value)){
            return (displayName + " length should between 8 to 15 chars" + "\n");
		}
		return "";
	}
	
	FormValidator.prototype.isValidPageURL = function(value, displayName){
		
		if(this.isEmpty(value)){
            return (displayName + " should not be blank" + "\n");
		}
		
		if(!this.haveAllowedSpecialChars(value, "./")){
            return (displayName + " has only allowed special chars[./]" + "\n");
		}
		
		if(!this.isLengthBetweenRange(5, 100, value)){
            return (displayName + " length should between 5 to 100 chars" + "\n");
		}
		
		if(this.haveNotAllowedCharacters(value, "0123456789")){
            return (displayName + " should not contain numbers" + "\n");
		}
		return "";
	}
	
	FormValidator.prototype.isValidDescription = function(value, displayName) {
		
		if(this.isEmpty(value)){
            return (displayName + " should not be blank" + "\n");
		}
		//if(!this.haveAllowedSpecialChars(value, "\".,':;?-_+=&%@!#$*()[]{}|\/~`<> ")){
		//	return (displayName+" has only allowed special chars[. ]"+"\n");
		//}
		if(!this.isLengthBetweenRange(4, 500, value)){
            return (displayName + " length should between 4 to 100 chars" + "\n");
		}
		if(this.isOnlyContain(value, ". ")){
            return (displayName + " should not only contains [. ]" + "\n");
		}
		return "";
	}
	
	FormValidator.prototype.isValidPersonName=function(value, displayName) {

		if(this.isEmpty(value)){
            return (displayName + " should not be blank" + "\n");
		}
		var a = value.charAt(0);
		if(!((a >= 'a' && a <='z')|| (a >= 'A' && a <= 'Z'))){
            return (displayName + " should begin with Alphabet" + "\n");
		}
		if(!this.haveAllowedSpecialChars(value, ". ")){
            return (displayName + " has only allowed special chars ' and space" + "\n");
		}
		if(!this.isLengthBetweenRange(2, 50, value)){
            return (displayName + " length should between 2 to 50 chars" + "\n");
		}
		if(this.haveNotAllowedCharacters(value, "0123456789")){
            return (displayName + " should not contain numbers" + "\n");
		}
		return "";
	}

    FormValidator.prototype.isValidOrganizationName = function (value, displayName) {

        if (this.isEmpty(value)) {
            return (displayName + " should not be blank" + "\n");
        }
        var a = value.charAt(0);
        if (!((a >= 'a' && a <= 'z') || (a >= 'A' && a <= 'Z'))) {
            return (displayName + " should begin with Alphabet" + "\n");
        }
        if (!this.haveAllowedSpecialChars(value, ".,&- ")) {
            return (displayName + " has only allowed special chars '.,&-' and space" + "\n");
        }
        if (!this.isLengthBetweenRange(2, 50, value)) {
            return (displayName + " length should between 2 to 50 chars" + "\n");
        }
        if (this.haveNotAllowedCharacters(value, "0123456789")) {
            return (displayName + " should not contain numbers" + "\n");
        }
        return "";
    }

	FormValidator.prototype.isValidNonMandatoryComboValue=function(value, displayName) {

		if(this.isEmpty(value)){
            return (displayName + " should be selected" + "\n");
		}
		if(this.isValueNumeric(value)){
			if(parseInt(value) < 0){
                return (displayName + " should be selected" + "\n");
			}
			return "";
		}
        return (displayName + " should be selected" + "\n");
	}
	
	FormValidator.prototype.isValidMandatoryComboValue=function(value, displayName) {

		if(this.isEmpty(value)){
            return (displayName + " should be selected" + "\n");
		}
		if(this.isValueNumeric(value)){
			if(parseInt(value) < 1){
                return (displayName + " should be selected" + "\n");
			}
			return "";
		}
        return (displayName + " should be selected" + "\n");
	}
	
	FormValidator.prototype.isAlphabet = function(value, displayName) {
		for(var index=0; index < value.length; index++){
			if(!((value.charAt(index) >= 'a' && value.charAt(index) <= 'z') || (value.charAt(index) >= 'A' && value.charAt(index) <= 'Z'))){
                return (displayName + " should be only Alphabets" + "\n");
			}
		}
		return "";
	}
	
	FormValidator.prototype.isValidTelephone = function(value, displayName){
		
		if(this.isEmpty(value)){
            return (displayName + " should be not blank" + "\n");
		}
		if(value.indexOf("-") < 0){
            return (displayName + " should contain -" + "\n");
		}
		
		var splittedTelephone = value.split("-");
		
		if(!(this.isValueNumeric(splittedTelephone[0]) || this.isValueNumeric(splittedTelephone[1]))){
            return (displayName + " contains only numbers except -" + "\n");
		}
		
		if(!(splittedTelephone[0].length >= 3 && splittedTelephone[0].length <=5)){
            return (displayName + " first part contains 3 to 5 numbers only" + "\n");
		}
		
		if(!(splittedTelephone[1].length >= 5 && splittedTelephone[1].length <=10)){
            return (displayName + " second part contains 5 to 10 numbers only" + "\n");
		}
	 
		return "";
	}
	
	FormValidator.prototype.isValidMobile = function(value, displayName){
		if(this.isEmpty(value)){
            return (displayName + " should not be blank" + "\n");
		}
		
		if(!this.isValueNumeric(value)){
            return (displayName + " should be only numbers" + "\n");
		}
		
		if(value.length!=10){
            return (displayName + " length should be 10" + "\n");
		}
		
		if(value.charAt(0)=='0'){
            return (displayName + " should not start with 0" + "\n");
		}
		 
		return "";
	}
	
	FormValidator.prototype.isValidEmail = function(value, displayName){
		
//		var pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/g;
//		if(pattern.test(value)){
//			return "";
//		}
//		return (displayName+" should be valid"+"\n");
		
		if(this.isEmpty(""+value)){
            return (displayName + " should not be blank" + "\n");
		}
		
		if(this.isValidMail(value)){
			return "";
		}
        return (displayName + " should be valid" + "\n");
		
		 
	}
	
	FormValidator.prototype.isValidMail = function(email) {
	    // If no email or wrong value gets passed in (or nothing is passed in), immediately return false.
	 
	    if(typeof email !== 'string' || email.indexOf('@') === -1) return false;

	    
	    // Get email parts
	    var emailParts = email.split('@'),
	        emailName = emailParts[0],
	        emailDomain = emailParts[1],
	        emailDomainParts = emailDomain.split('.'),
	        validChars = 'abcdefghijklmnopqrstuvwxyz.0123456789_';

	 // Must be at least one char before @ and 3 chars after
	    if(emailName.length < 1 || emailDomain.length < 3) {
//	        alert("Wrong number of characters before or after @ sign");
	        return false;
	    }
	    
	    if(emailName.charAt(0)=="." || emailName.charAt(emailName.length-1)=="." 
	    	|| emailName.charAt(0)=="_" || emailName.charAt(emailName.length-1)=="_"){
	    	return false;
	    }
	    
	    if(email.indexOf("..")!=-1 || email.indexOf("._")!=-1 || email.indexOf("_.")!=-1   || email.indexOf("__")!=-1){
	    	return false;
	    }
	    	
	    // There must be exactly 2 parts
	    if(emailParts.length !== 2) {
//	        alert("Wrong number of @ signs");
	        return false;
	    }

	    // Domain must include but not start with .
	    if(emailDomain.indexOf('.') <= 0) {
//	        alert("Domain must include but not start with .");
	        return false;
	    }

	    // emailName must only include valid chars
	    for (var i = emailName.length - 1; i >= 0; i--) {
	        if(validChars.indexOf(emailName[i]) < 0) {
//	            alert("Invalid character in name section");
	            return false;
	        }
	    };

	    // emailDomain must only include valid chars
	    for (var i = emailDomain.length - 1; i >= 0; i--) {
	        if(validChars.indexOf(emailDomain[i]) < 0) {
//	            alert("Invalid character in domain section");
	            return false;
	        }
	    };

	    // Domain's last . should be 2 chars or more from the end
	    if(emailDomainParts[emailDomainParts.length - 1].length < 2) {
//	        alert("Domain's last . should be 2 chars or more from the end");
	        return false;   
	    }

//	    alert("Email address seems valid");
	    return true;
	}
	
		FormValidator.prototype.isValidDate = function(value, displayName){
		var parms = value.split(/[\.\-\/]/);
		var yyyy = parseInt(parms[2]);
		var mm = parseInt(parms[1]);
		var dd = parseInt(parms[0]);
		var date = new Date(yyyy, mm - 1, dd, 0, 0, 0, 0);
		if (mm === (date.getMonth() + 1) && dd === date.getDate() && yyyy === date.getFullYear()) {
			return "";
		}
        return (displayName + " should be valid" + "\n");
	}
		
	FormValidator.prototype.isValidBloodGroup = function(value, displayName) {
		if(this.isEmpty(value)){
            return (displayName + " should not be blank" + "\n");
		}
		if(value=="A+" || value=="A-" || value=="B+" || value=="B-" || value=="AB+" || value=="AB-" || value=="O+" || value=="O-"){
			return "";
		}
        return (displayName + " should be valid[A+,A-....]" + "\n");
	}

    FormValidator.prototype.isMandetoryNumeric = function (value, displayName) {
        if (this.isEmpty(value)) {
            return (displayName + " should not be blank" + "\n");
        }
        if (!this.isValueNumeric(value)) {
            return (displayName + " should be Numeric" + "\n");
        }
        return "";
    }
	 FormValidator.prototype.isNumeric = function(value, displayName){

         if (!this.isValueNumeric(value)) {
                return (displayName + " should be Numeric" + "\n");
			}
			return "";
		}
	
	FormValidator.prototype.isOnlyContain=function(value, characters){
		 for(var index=0; index < value.length; index++){
			 var character = value.charAt(index);
			 if(characters.indexOf(character)==-1){
					return false;
				}
		 }
		 return true;
	}
	
	FormValidator.prototype.haveNotAllowedCharacters= function(value, notAllowedCharacters) {
		 for(var index=0; index < value.length; index++){
			 var character = value.charAt(index);
			 if(notAllowedCharacters.indexOf(character) > -1){
				 // not allowed char found...
					return true;
			 }
		 }
		 return false;
	}
	
 FormValidator.prototype.isLengthBetweenRange = function(lowerLimit, upperLimit, value){
		
		return (value.length >= lowerLimit && value.length <= upperLimit)? true : false;
	}
	
 FormValidator.prototype.haveAllowedSpecialChars = function(value, allowedSpecialChars){
		 for(var index=0; index < value.length; index++){
			 var character = value.charAt(index);
			 if(this.isSpecialCharacter(character)){
				 if(allowedSpecialChars.indexOf(character)==-1){
						return false;
					}
			 }
		 }
		 return true;
	}
	
 FormValidator.prototype.isSpecialCharacter = function(character)
	{
		var specialCharacters = "~`!@#$%^&*()_-+=\\|;:'\",<.>/?[]{} ";
		if(specialCharacters.indexOf(character)==-1){
			return false;
		}
		return true;
	}
 
 FormValidator.prototype.isEmpty = function(value){
		if(this.isUndefined(value) || this.isNull(value) || this.isBlank(value)){
			return true;
		}
		return false;
	}
	
 
 FormValidator.prototype.isNull = function(value){
		if(value == null){
			return true;
		}
		return false;
	}
	
 FormValidator.prototype.isBlank = function(value){
		if(value.trim() == ""){
			return true;
		}
		return false;
	}
	
 FormValidator.prototype.isUndefined = function(value){
		if(value == undefined){
			return true;
		}
		return false;
	}
 
 FormValidator.prototype.isValueNumeric = function(k){
	 
	 var value = "A"+k;
	 	for(var index=1; index < value.length ; index++){
	 		if(!(value.charAt(index) >= '0' && value.charAt(index) <= '9')){
	 			return false;
	 		}
	 	}
	 	return true;
	}
}