﻿//search criteria
function getPackages() {
    var message = "";
    var ddlCategory = $('#ddlCategory :selected').text();
    var valCategory = $('#ddlCategory').val();

    var ddlDuration = $('#ddlDuration :selected').text();
    var valDuration = $('#ddlDuration').val();

    var destination = $('#destination').val();

    if (ddlCategory == "Select Category" && destination == "") {
        message = "atleast enter destination or select category!";
    }

    if (message == "") {
        window.location.href = '/Products?CategoryId=' + valCategory + '&DurationId=' + valDuration + '&Destination=' + destination + '&page=1';
    }
    else {
        alert(message);
        return false;
    }
}
