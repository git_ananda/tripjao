﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TripJao.Models;

namespace TripJao.Controllers
{
    public class AboutUsController : Controller
    {
        // GET: AboutUs
        public ActionResult Index()
        {
            string errMsg = string.Empty;

            Products products = new Products();
            ViewBag.Products = products.GetList(ProductTypes.Promotion, ref errMsg);

            Region regions = new Region();
            ViewBag.Regions = regions.GetList();

            Category categories = new Category();
            ViewBag.CategorySelectList = categories.GetSelectList();

            ViewBag.DurationsSelectList = ProductDuration.GetSelectList();

            if (!string.IsNullOrEmpty(errMsg))
            {
                TempData["message"] = errMsg;
            }
            return View();
        }
    }
}