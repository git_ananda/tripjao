﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TripJao.Models;

namespace TripJao.Controllers.Admin
{
    public class CitiesController : Controller
    {
        private TripJaoEntities db = new TripJaoEntities();

        // GET: Cities
        public ActionResult Index()
        {
            var tJ_Cities = db.TJ_Cities.Include(t => t.TJ_Region).Include(t => t.TJ_Zones).Include(t => t.TJ_Zones1).Include(t => t.TJ_Country);
            return View(tJ_Cities.ToList());
        }

        // GET: Cities/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TJ_Cities tJ_Cities = db.TJ_Cities.Find(id);
            if (tJ_Cities == null)
            {
                return HttpNotFound();
            }
            return View(tJ_Cities);
        }

        // GET: Cities/Create
        public ActionResult Create()
        {
            ViewBag.RegionId = new SelectList(db.TJ_Region, "Id", "Name");
            ViewBag.ZoneId = new SelectList(db.TJ_Zones, "Id", "Name");
            ViewBag.ZoneId = new SelectList(db.TJ_Zones, "Id", "Name");
            ViewBag.CountryId = new SelectList(db.TJ_Country, "ID", "CountryCode");
            return View();
        }

        // POST: Cities/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,RegionId,ZoneId,Name,Code,CountryId")] TJ_Cities tJ_Cities)
        {
            if (ModelState.IsValid)
            {
                db.TJ_Cities.Add(tJ_Cities);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.RegionId = new SelectList(db.TJ_Region, "Id", "Name", tJ_Cities.RegionId);
            ViewBag.ZoneId = new SelectList(db.TJ_Zones, "Id", "Name", tJ_Cities.ZoneId);
            ViewBag.ZoneId = new SelectList(db.TJ_Zones, "Id", "Name", tJ_Cities.ZoneId);
            ViewBag.CountryId = new SelectList(db.TJ_Country, "ID", "CountryCode", tJ_Cities.CountryId);
            return View(tJ_Cities);
        }

        // GET: Cities/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TJ_Cities tJ_Cities = db.TJ_Cities.Find(id);
            if (tJ_Cities == null)
            {
                return HttpNotFound();
            }
            ViewBag.RegionId = new SelectList(db.TJ_Region, "Id", "Name", tJ_Cities.RegionId);
            ViewBag.ZoneId = new SelectList(db.TJ_Zones, "Id", "Name", tJ_Cities.ZoneId);
            ViewBag.ZoneId = new SelectList(db.TJ_Zones, "Id", "Name", tJ_Cities.ZoneId);
            ViewBag.CountryId = new SelectList(db.TJ_Country, "ID", "CountryCode", tJ_Cities.CountryId);
            return View(tJ_Cities);
        }

        // POST: Cities/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,RegionId,ZoneId,Name,Code,CountryId")] TJ_Cities tJ_Cities)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tJ_Cities).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("City","Admin");
            }
            ViewBag.RegionId = new SelectList(db.TJ_Region, "Id", "Name", tJ_Cities.RegionId);
            ViewBag.ZoneId = new SelectList(db.TJ_Zones, "Id", "Name", tJ_Cities.ZoneId);
            ViewBag.ZoneId = new SelectList(db.TJ_Zones, "Id", "Name", tJ_Cities.ZoneId);
            ViewBag.CountryId = new SelectList(db.TJ_Country, "ID", "CountryCode", tJ_Cities.CountryId);
            return View(tJ_Cities);
        }

        // GET: Cities/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TJ_Cities tJ_Cities = db.TJ_Cities.Find(id);
            if (tJ_Cities == null)
            {
                return HttpNotFound();
            }
            return View(tJ_Cities);
        }

        // POST: Cities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            TJ_Cities tJ_Cities = db.TJ_Cities.Find(id);
            db.TJ_Cities.Remove(tJ_Cities);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
