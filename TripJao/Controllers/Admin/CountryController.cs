﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TripJao.Models;

namespace TripJao.Controllers.Admin
{
    public class CountryController : Controller
    {
        private TripJaoEntities db = new TripJaoEntities();

        // GET: Country
        public ActionResult Index()
        {
            var tJ_Country = db.TJ_Country.Include(t => t.TJ_Region);
            return View(tJ_Country.ToList());
        }

        // GET: Country/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TJ_Country tJ_Country = db.TJ_Country.Find(id);
            if (tJ_Country == null)
            {
                return HttpNotFound();
            }
            return View(tJ_Country);
        }

        // GET: Country/Create
        public ActionResult Create()
        {
            ViewBag.RegionId = new SelectList(db.TJ_Region, "Id", "Name");
            return View();
        }

        // POST: Country/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,CountryCode,CountryName,RegionId")] TJ_Country tJ_Country)
        {
            if (ModelState.IsValid)
            {
                db.TJ_Country.Add(tJ_Country);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.RegionId = new SelectList(db.TJ_Region, "Id", "Name", tJ_Country.RegionId);
            return View(tJ_Country);
        }

        // GET: Country/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TJ_Country tJ_Country = db.TJ_Country.Find(id);
            if (tJ_Country == null)
            {
                return HttpNotFound();
            }
            ViewBag.RegionId = new SelectList(db.TJ_Region, "Id", "Name", tJ_Country.RegionId);
            return View(tJ_Country);
        }

        // POST: Country/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,CountryCode,CountryName,RegionId")] TJ_Country tJ_Country)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tJ_Country).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("CountryMaster", "Admin");
            }
            ViewBag.RegionId = new SelectList(db.TJ_Region, "Id", "Name", tJ_Country.RegionId);
            return View(tJ_Country);
        }

        // GET: Country/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TJ_Country tJ_Country = db.TJ_Country.Find(id);
            if (tJ_Country == null)
            {
                return HttpNotFound();
            }
            return View(tJ_Country);
        }

        // POST: Country/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            TJ_Country tJ_Country = db.TJ_Country.Find(id);
            db.TJ_Country.Remove(tJ_Country);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
