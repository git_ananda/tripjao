﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TripJao.Models;

namespace TripJao.Controllers.Admin
{
    public class JobCategoriesController : Controller
    {
        private TripJaoEntities db = new TripJaoEntities();

        // GET: JobCategories
        public async Task<ActionResult> Index()
        {
            return View(await db.TJ_Job_Category.ToListAsync());
        }

        // GET: JobCategories/Details/5
        public async Task<ActionResult> Details(long? id)
        {
            if (Session["User"] == null /*&& string.IsNullOrEmpty(Session["User"].ToString())*/)
            {
                return RedirectToAction("Index", "Admin");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TJ_Job_Category jobCategory = await db.TJ_Job_Category.FindAsync(id);
            if (jobCategory == null)
            {
                return HttpNotFound();
            }
            return View(jobCategory);
        }

        // GET: JobCategories/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: JobCategories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,JobCategoryName")] TJ_Job_Category jobCategory)
        {
            if (Session["User"] == null /*&& string.IsNullOrEmpty(Session["User"].ToString())*/)
            {
                return RedirectToAction("Index", "Admin");
            }
            if (ModelState.IsValid)
            {
                db.TJ_Job_Category.Add(jobCategory);
                await db.SaveChangesAsync();
                return RedirectToAction("JobCategories", "Admin");
            }

            return View(jobCategory);
        }

        // GET: JobCategories/Edit/5
        public async Task<ActionResult> Edit(long? id)
        {
            if (Session["User"] == null /*&& string.IsNullOrEmpty(Session["User"].ToString())*/)
            {
                return RedirectToAction("Index", "Admin");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TJ_Job_Category tj_jobCategory = await db.TJ_Job_Category.FindAsync(id);
            if (tj_jobCategory == null)
            {
                return HttpNotFound();
            }

            
            return View(tj_jobCategory);
        }

        // POST: JobCategories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,Job_Cat_Name")] TJ_Job_Category jobCategory)
        {
            if (Session["User"] == null /*&& string.IsNullOrEmpty(Session["User"].ToString())*/)
            {
                return RedirectToAction("Index", "Admin");
            }
            if (ModelState.IsValid)
            {
                db.Entry(jobCategory).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("JobCategories", "Admin");
            }
            return View(jobCategory);
        }

        // GET: JobCategories/Delete/5
        public async Task<ActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TJ_Job_Category jobCategory = await db.TJ_Job_Category.FindAsync(id);
            if (jobCategory == null)
            {
                return HttpNotFound();
            }
            return View(jobCategory);
        }

        // POST: JobCategories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(long id)
        {
            TJ_Job_Category jobCategory = await db.TJ_Job_Category.FindAsync(id);
            db.TJ_Job_Category.Remove(jobCategory);
            await db.SaveChangesAsync();
            return RedirectToAction("JobCategories", "Admin");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
