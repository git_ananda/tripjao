﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TripJao.Models;

namespace TripJao.Controllers.Admin
{
    public class JobVacanciesController : Controller
    {
        private TripJaoEntities db = new TripJaoEntities();

        // GET: JobVacancies
        public ActionResult Index()
        {
            var tJ_JobVacancies = db.TJ_JobVacancies.ToList();
            return View(tJ_JobVacancies.ToList());
        }

        // GET: JobVacancies/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TJ_JobVacancies tj_JobVacancies = db.TJ_JobVacancies.Find(id);
            if (tj_JobVacancies == null)
            {
                return HttpNotFound();
            }
            ViewBag.JobCategoryId = new SelectList(db.TJ_Job_Category, "ID", "CountryCode", tj_JobVacancies.Job_Category_Id);
            return View(tj_JobVacancies);
        }

        // GET: JobVacancies/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: JobVacancies/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,JobCode,JobTitle,JobQualification,Job_Category_Id,JobDesignation,MinExperience,MaxExperience,JobCTC,JobDescription")] TJ_JobVacancies tj_JobVacancies)
        {
            if (ModelState.IsValid)
            {
                db.TJ_JobVacancies.Add(tj_JobVacancies);
                db.SaveChanges();

                return RedirectToAction("Index");
            }

            return View(tj_JobVacancies);
        }

        // GET: JobVacancies/Edit/5
        public ActionResult Edit(long? id)
        {
            if (Session["User"] == null /*&& string.IsNullOrEmpty(Session["User"].ToString())*/)
            {
                return RedirectToAction("Index","Admin");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TJ_JobVacancies tj_JobVacancies = db.TJ_JobVacancies.Find(id);
            if (tj_JobVacancies == null)
            {
                return HttpNotFound();
            }
            ViewBag.JobCategoryId = new SelectList(db.TJ_Job_Category, "ID", "Job_Cat_Name", tj_JobVacancies.Job_Category_Id);
            return View(tj_JobVacancies);
        }

        // POST: JobVacancies/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,JobCode,JobTitle,JobQualification,Job_Category_Id,JobDesignation,MinExperience,MaxExperience,JobCTC,JobDescription")] TJ_JobVacancies tj_JobVacancies)
        {
            if (Session["User"] == null /*&& string.IsNullOrEmpty(Session["User"].ToString())*/)
            {
                return RedirectToAction("Index", "Admin");
            }

            if (ModelState.IsValid)
            {
                db.Entry(tj_JobVacancies).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("JobVacancies", "Admin");
            }
            ViewBag.JobCategoryId = new SelectList(db.TJ_Job_Category, "ID", "Job_Cat_Name", tj_JobVacancies.Job_Category_Id);
            return View(tj_JobVacancies);
        }

        // GET: JobVacancies/Delete/5
        public ActionResult Delete(long? id)
        {
            if (Session["User"] == null /*&& string.IsNullOrEmpty(Session["User"].ToString())*/)
            {
                return RedirectToAction("Index", "Admin");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TJ_JobVacancies tj_JobVacancies = db.TJ_JobVacancies.Find(id);
            if (tj_JobVacancies == null)
            {
                return HttpNotFound();
            }
            return View(tj_JobVacancies);
        }

        // POST: JobVacancies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            if (Session["User"] == null /*&& string.IsNullOrEmpty(Session["User"].ToString())*/)
            {
                return RedirectToAction("Index", "Admin");
            }
            TJ_JobVacancies tj_JobVacancies = db.TJ_JobVacancies.Find(id);
            db.TJ_JobVacancies.Remove(tj_JobVacancies);
            db.SaveChanges();
            return RedirectToAction("JobVacancies", "Admin");
        }
    }
}
