﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TripJao.Models;

namespace TripJao.Controllers.Admin
{
    public class ProductController : Controller
    {
        private TripJaoEntities db = new TripJaoEntities();

        // GET: Product
        public ActionResult Index()
        {
            var tJ_Products = db.TJ_Products.Include(t => t.TJ_Category).Include(t => t.TJ_Cities).Include(t => t.TJ_Iternary).Include(t => t.TJ_Region).Include(t => t.TJ_Season).Include(t => t.TJ_Zones);
            return View(tJ_Products.ToList());
        }

        // GET: Product/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TJ_Products tJ_Products = db.TJ_Products.Find(id);
            if (tJ_Products == null)
            {
                return HttpNotFound();
            }
            return View(tJ_Products);
        }

        // GET: Product/Create
        public ActionResult Create()
        {
            ViewBag.IternaryCategory = new SelectList(db.TJ_Category, "Id", "Name");
            ViewBag.CityId = new SelectList(db.TJ_Cities, "Id", "Name");
            ViewBag.IternaryId = new SelectList(db.TJ_Iternary, "Id", "IternaryCode");
            ViewBag.RegionId = new SelectList(db.TJ_Region, "Id", "Name");
            ViewBag.SeasonId = new SelectList(db.TJ_Season, "Id", "Name");
            ViewBag.ZoneId = new SelectList(db.TJ_Zones, "Id", "Name");
            return View();
        }

        // POST: Product/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,BusinessType,SeasonId,ValidFrom,ValidTo,IternaryId,IternaryDescription,ProductIternaryCode,NoOfDays,NoOfNights,IternaryStatus,Remark,ZoneId,CityId,RegionId,IternaryCategory,Source,Destination,Thumbnail,LocationPath,TopPromotion,ProductName")] TJ_Products tJ_Products)
        {
            if (ModelState.IsValid)
            {
                db.TJ_Products.Add(tJ_Products);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IternaryCategory = new SelectList(db.TJ_Category, "Id", "Name", tJ_Products.IternaryCategory);
            ViewBag.CityId = new SelectList(db.TJ_Cities, "Id", "Name", tJ_Products.CityId);
            ViewBag.IternaryId = new SelectList(db.TJ_Iternary, "Id", "IternaryCode", tJ_Products.IternaryId);
            ViewBag.RegionId = new SelectList(db.TJ_Region, "Id", "Name", tJ_Products.RegionId);
            ViewBag.SeasonId = new SelectList(db.TJ_Season, "Id", "Name", tJ_Products.SeasonId);
            ViewBag.ZoneId = new SelectList(db.TJ_Zones, "Id", "Name", tJ_Products.ZoneId);
            return View(tJ_Products);
        }

        // GET: Product/Edit/5
        public ActionResult Edit(long? id, string viewName)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TJ_Products tJ_Products = db.TJ_Products.Find(id);
            if (tJ_Products == null)
            {
                return HttpNotFound();
            }
            ViewBag.IternaryCategory = new SelectList(db.TJ_Category, "Id", "Name", tJ_Products.IternaryCategory);
            ViewBag.CityId = new SelectList(db.TJ_Cities, "Id", "Name", tJ_Products.CityId);
            ViewBag.IternaryId = new SelectList(db.TJ_Iternary, "Id", "IternaryCode", tJ_Products.IternaryId);
            ViewBag.RegionId = new SelectList(db.TJ_Region, "Id", "Name", tJ_Products.RegionId);
            ViewBag.SeasonId = new SelectList(db.TJ_Season, "Id", "Name", tJ_Products.SeasonId);
            ViewBag.ZoneId = new SelectList(db.TJ_Zones, "Id", "Name", tJ_Products.ZoneId);
            ViewBag.CountryId = new SelectList(db.TJ_Country, "ID", "CountryName", tJ_Products.CountryId);
            ViewBag.BusinessType = new SelectList(Types.GetBusinessTypesList(), "Id", "Name", tJ_Products.BusinessType);
            ViewBag.TopPromotion = new SelectList(ProductTopPromotionValue.GetList(), "Id", "Name", tJ_Products.TopPromotion);
            ViewBag.IternaryStatus = new SelectList(Iternaries.ItenaryStatusValue.GetList(), "Id", "Name", tJ_Products.IternaryStatus);
            tJ_Products.Categories = db.TJ_Product_Categories.Where(x => x.ProductId == tJ_Products.Id).Select(x => new CheckBoxes
            {
                Id = x.Id,
                Text = x.Name,
                Checked = x.Checked ?? true
            }).ToList();
            tJ_Products.Inclusions = tJ_Products.TJ_Product_Inclusions.Select(x => new CheckBoxes
            {
                Id = x.Id,
                Text = x.Name,
                Checked = x.Checked ?? true
            }).ToList();
            tJ_Products.Exclusions = tJ_Products.TJ_Product_Exclusions.Select(x => new CheckBoxes
            {
                Id = x.Id,
                Text = x.Name,
                Checked = x.Checked ?? true
            }).ToList();
            tJ_Products.Iternaries = tJ_Products.TJ_Product_Routes.Select(x => new ProductRoutes
            {
                Id = x.Id,
                City = x.City,
                Description = x.Description,
                DropPoint = x.DropPoint,
                PickupPoint = x.PickupPoint,
                ProductId = x.ProductId,
                Day = x.Day
            }).OrderBy(x => x.Day).ToList();

            tJ_Products.Price = tJ_Products.TJ_Product_Prices.FirstOrDefault();

            tJ_Products.ViewName = viewName;
            return View(tJ_Products);
        }

        // POST: Product/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,BusinessType,SeasonId,ValidFrom,ValidTo,IternaryId,IternaryDescription,ProductIternaryCode,NoOfDays,NoOfNights,IternaryStatus,Remark,ZoneId,CityId,RegionId,IternaryCategory,Source,Destination,Thumbnail,LocationPath,TopPromotion,ProductName,Categories,Inclusions,Exclusions,ViewName,Iternaries,CountryId,Price")] TJ_Products tJ_Products)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tJ_Products).State = EntityState.Modified;
                db.SaveChanges();

                if (tJ_Products.Categories != null)
                {
                    foreach (var cat in tJ_Products.Categories.ToList())
                    {
                        TJ_Product_Categories catPro = db.TJ_Product_Categories.Where(x => x.Id == cat.Id).FirstOrDefault();
                        catPro.Checked = cat.Checked;
                        catPro.Name = cat.Text;
                        db.SaveChanges();
                    }
                }

                if (tJ_Products.Inclusions != null)
                {
                    foreach (var inc in tJ_Products.Inclusions.ToList())
                    {
                        TJ_Product_Inclusions inclusion = db.TJ_Product_Inclusions.Where(x => x.Id == inc.Id).FirstOrDefault();
                        inclusion.Checked = inc.Checked;
                        inclusion.Name = inc.Text;
                        db.SaveChanges();
                    }
                }

                if (tJ_Products.Exclusions != null)
                {
                    foreach (var exc in tJ_Products.Exclusions.ToList())
                    {
                        TJ_Product_Exclusions exclusion = db.TJ_Product_Exclusions.Where(x => x.Id == exc.Id).FirstOrDefault();
                        exclusion.Checked = exc.Checked;
                        exclusion.Name = exc.Text;
                        db.SaveChanges();
                    }
                }

                if (tJ_Products.Iternaries != null)
                {
                    foreach (var iter in tJ_Products.Iternaries.ToList())
                    {
                        TJ_Product_Routes iternary = db.TJ_Product_Routes.Where(x => x.Id == iter.Id).FirstOrDefault();
                        if (iter.City != null)
                            iternary.City = iter.City;
                        if (iter.Description != null)
                            iternary.Description = iter.Description;
                        if (iter.DropPoint != null)
                            iternary.DropPoint = iter.DropPoint;
                        if (iter.PickupPoint != null)
                            iternary.PickupPoint = iter.PickupPoint;
                        db.SaveChanges();
                    }
                }
                var productPrice = db.TJ_Product_Prices.Where(x => x.ProductId == tJ_Products.Id).FirstOrDefault();
                productPrice.CostPrice = tJ_Products.Price.CostPrice;
                productPrice.Discount = tJ_Products.Price.Discount;
                productPrice.DiscountedPrice = tJ_Products.Price.DiscountedPrice;
                db.SaveChanges();
                return RedirectToAction(tJ_Products.ViewName, "Admin");
            }

            ViewBag.IternaryCategory = new SelectList(db.TJ_Category, "Id", "Name", tJ_Products.TJ_Product_Categories);
            ViewBag.CityId = new SelectList(db.TJ_Cities, "Id", "Name", tJ_Products.CityId);
            ViewBag.IternaryId = new SelectList(db.TJ_Iternary, "Id", "IternaryCode", tJ_Products.IternaryId);
            ViewBag.RegionId = new SelectList(db.TJ_Region, "Id", "Name", tJ_Products.RegionId);
            ViewBag.SeasonId = new SelectList(db.TJ_Season, "Id", "Name", tJ_Products.SeasonId);
            ViewBag.CountryId = new SelectList(db.TJ_Country, "ID", "CountryName", tJ_Products.CountryId);
            ViewBag.ZoneId = new SelectList(db.TJ_Zones, "Id", "Name", tJ_Products.ZoneId);
            ViewBag.BusinessType = new SelectList(Types.GetBusinessTypesList(), "Id", "Name", tJ_Products.BusinessType);
            ViewBag.TopPromotion = new SelectList(ProductTopPromotionValue.GetList(), "Id", "Name", tJ_Products.TopPromotion);
            tJ_Products.Categories = db.TJ_Product_Categories.Where(x => x.ProductId == tJ_Products.Id).Select(x => new CheckBoxes
            {
                Id = x.Id,
                Text = x.Name,
                Checked = x.Checked ?? true
            }).ToList();
            tJ_Products.Inclusions = tJ_Products.TJ_Product_Inclusions.Select(x => new CheckBoxes
            {
                Id = x.Id,
                Text = x.Name,
                Checked = x.Checked ?? true
            }).ToList();
            tJ_Products.Exclusions = tJ_Products.TJ_Product_Exclusions.Select(x => new CheckBoxes
            {
                Id = x.Id,
                Text = x.Name,
                Checked = x.Checked ?? true
            }).ToList();
            tJ_Products.Iternaries = tJ_Products.TJ_Product_Routes.Select(x => new ProductRoutes
            {
                Id = x.Id,
                City = x.City,
                Description = x.Description,
                DropPoint = x.DropPoint,
                PickupPoint = x.PickupPoint,
                ProductId = x.ProductId,
                Day = x.Day
            }).OrderBy(x => x.Day).ToList();
            return View(tJ_Products);
        }

        // GET: Product/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TJ_Products tJ_Products = db.TJ_Products.Find(id);
            if (tJ_Products == null)
            {
                return HttpNotFound();
            }
            return View(tJ_Products);
        }

        // POST: Product/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            TJ_Products tJ_Products = db.TJ_Products.Find(id);
            db.TJ_Products.Remove(tJ_Products);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
