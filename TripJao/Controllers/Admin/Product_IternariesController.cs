﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TripJao.Models;

namespace TripJao.Controllers.Admin
{
    public class Product_IternariesController : Controller
    {
        private TripJaoEntities db = new TripJaoEntities();

        // GET: Product_Iternaries
        public ActionResult Index()
        {
            var tJ_Product_Routes = db.TJ_Product_Routes.Include(t => t.TJ_Products);
            return View(tJ_Product_Routes.ToList());
        }

        // GET: Product_Iternaries/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TJ_Product_Routes tJ_Product_Routes = db.TJ_Product_Routes.Find(id);
            if (tJ_Product_Routes == null)
            {
                return HttpNotFound();
            }
            return View(tJ_Product_Routes);
        }

        // GET: Product_Iternaries/Create
        public ActionResult Create()
        {
            ViewBag.ProductId = new SelectList(db.TJ_Products, "Id", "ProductIternaryCode");
            return View();
        }

        // POST: Product_Iternaries/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ProductId,City,PickupPoint,DropPoint,Description,Day")] TJ_Product_Routes tJ_Product_Routes)
        {
            if (ModelState.IsValid)
            {
                db.TJ_Product_Routes.Add(tJ_Product_Routes);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ProductId = new SelectList(db.TJ_Products, "Id", "BusinessType", tJ_Product_Routes.ProductId);
            return View(tJ_Product_Routes);
        }

        // GET: Product_Iternaries/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TJ_Product_Routes tJ_Product_Routes = db.TJ_Product_Routes.Find(id);
            if (tJ_Product_Routes == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProductId = new SelectList(db.TJ_Products, "Id", "ProductIternaryCode", tJ_Product_Routes.ProductId);
            return View(tJ_Product_Routes);
        }

        // POST: Product_Iternaries/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ProductId,City,PickupPoint,DropPoint,Description,Day")] TJ_Product_Routes tJ_Product_Routes)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tJ_Product_Routes).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ProductDayWisePlan", "Admin");
            }
            ViewBag.ProductId = new SelectList(db.TJ_Products, "Id", "BusinessType", tJ_Product_Routes.ProductId);
            return View(tJ_Product_Routes);
        }

        // GET: Product_Iternaries/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TJ_Product_Routes tJ_Product_Routes = db.TJ_Product_Routes.Find(id);
            if (tJ_Product_Routes == null)
            {
                return HttpNotFound();
            }
            return View(tJ_Product_Routes);
        }

        // POST: Product_Iternaries/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            TJ_Product_Routes tJ_Product_Routes = db.TJ_Product_Routes.Find(id);
            db.TJ_Product_Routes.Remove(tJ_Product_Routes);
            db.SaveChanges();
            return RedirectToAction("ProductDayWisePlan", "Admin");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
