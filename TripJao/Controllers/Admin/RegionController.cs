﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TripJao.Models;

namespace TripJao.Controllers.Admin
{
    public class RegionController : Controller
    {
        private TripJaoEntities db = new TripJaoEntities();

        // GET: Region
        public ActionResult Index()
        {
            return View(db.TJ_Region.ToList());
        }

        // GET: Region/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TJ_Region tJ_Region = db.TJ_Region.Find(id);
            if (tJ_Region == null)
            {
                return HttpNotFound();
            }
            return View(tJ_Region);
        }

        // GET: Region/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Region/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Code")] TJ_Region tJ_Region)
        {
            if (ModelState.IsValid)
            {
                db.TJ_Region.Add(tJ_Region);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tJ_Region);
        }

        // GET: Region/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TJ_Region tJ_Region = db.TJ_Region.Find(id);
            if (tJ_Region == null)
            {
                return HttpNotFound();
            }
            return View(tJ_Region);
        }

        // POST: Region/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Code")] TJ_Region tJ_Region)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tJ_Region).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Region","Admin");
            }
            return View(tJ_Region);
        }

        // GET: Region/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TJ_Region tJ_Region = db.TJ_Region.Find(id);
            if (tJ_Region == null)
            {
                return HttpNotFound();
            }
            return View(tJ_Region);
        }

        // POST: Region/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            TJ_Region tJ_Region = db.TJ_Region.Find(id);
            db.TJ_Region.Remove(tJ_Region);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
