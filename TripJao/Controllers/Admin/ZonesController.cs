﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TripJao.Models;

namespace TripJao.Controllers.Admin
{
    public class ZonesController : Controller
    {
        private TripJaoEntities db = new TripJaoEntities();

        // GET: Zones
        public ActionResult Index()
        {
            var tJ_Zones = db.TJ_Zones.Include(t => t.TJ_Country).Include(t => t.TJ_Region);
            return View(tJ_Zones.ToList());
        }

        // GET: Zones/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TJ_Zones tJ_Zones = db.TJ_Zones.Find(id);
            if (tJ_Zones == null)
            {
                return HttpNotFound();
            }
            return View(tJ_Zones);
        }

        // GET: Zones/Create
        public ActionResult Create()
        {
            ViewBag.CountryId = new SelectList(db.TJ_Country, "ID", "CountryCode");
            ViewBag.RegionId = new SelectList(db.TJ_Region, "Id", "Name");
            return View();
        }

        // POST: Zones/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,RegionId,Name,Code,CountryId")] TJ_Zones tJ_Zones)
        {
            if (ModelState.IsValid)
            {
                db.TJ_Zones.Add(tJ_Zones);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CountryId = new SelectList(db.TJ_Country, "ID", "CountryCode", tJ_Zones.CountryId);
            ViewBag.RegionId = new SelectList(db.TJ_Region, "Id", "Name", tJ_Zones.RegionId);
            return View(tJ_Zones);
        }

        // GET: Zones/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TJ_Zones tJ_Zones = db.TJ_Zones.Find(id);
            if (tJ_Zones == null)
            {
                return HttpNotFound();
            }
            ViewBag.CountryId = new SelectList(db.TJ_Country, "ID", "CountryCode", tJ_Zones.CountryId);
            ViewBag.RegionId = new SelectList(db.TJ_Region, "Id", "Name", tJ_Zones.RegionId);
            return View(tJ_Zones);
        }

        // POST: Zones/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,RegionId,Name,Code,CountryId")] TJ_Zones tJ_Zones)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tJ_Zones).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CountryId = new SelectList(db.TJ_Country, "ID", "CountryCode", tJ_Zones.CountryId);
            ViewBag.RegionId = new SelectList(db.TJ_Region, "Id", "Name", tJ_Zones.RegionId);
            return View(tJ_Zones);
        }

        // GET: Zones/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TJ_Zones tJ_Zones = db.TJ_Zones.Find(id);
            if (tJ_Zones == null)
            {
                return HttpNotFound();
            }
            return View(tJ_Zones);
        }

        // POST: Zones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            TJ_Zones tJ_Zones = db.TJ_Zones.Find(id);
            db.TJ_Zones.Remove(tJ_Zones);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
