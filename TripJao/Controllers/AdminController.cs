﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TripJao.Models;
using TripJao.Utilities;

namespace TripJao.Controllers
{
    public class AdminController : Controller
    {
        TripJaoEntities db = new TripJaoEntities();

        public object ErrorMessage { get; private set; }

        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Login()
        {
            Session["User"] = "";
            return RedirectToAction("Index");

        }

        [HttpPost]
        public JsonResult KeepSessionAlive()
        {
            return new JsonResult { Data = "Success" };
        }

        [ValidateAntiForgeryToken]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Login(Users adminUser)
        {
            if (ModelState.IsValid)
            {
                bool status = false;
                var user = db.TJ_Users.Where(x => x.UserName.ToLower().Equals(adminUser.UserName.ToLower())).FirstOrDefault();
                if (user != null)
                {
                    var EncryptionKey = user.EncryptionKey;
                    //Password Hasing Process Call Helper Class Method    
                    var encodingPasswordString = PasswordEncDncUtility.EncodePassword(adminUser.Password, EncryptionKey);

                    if (user.Password.Equals(encodingPasswordString))
                    {
                        Session["User"] = user.FirstName ?? "" + " " + user.LastName ?? "";
                        status = true;
                    }
                    else
                    {
                        TempData["message"] = "Invalid User Name and Password";
                    }
                }
                if (!status)
                {
                    TempData["message"] = "Invalid User Name and Password";
                    Session["User"] = "";
                    return View("Index");
                }
                return RedirectToAction("Dashboard");
            }
            else
            {
                Session["User"] = "";
                return View("Index");
            }
        }

        [HttpGet]
        public ActionResult Logout()
        {
            Session.Abandon();
            Session["User"] = "";
            return RedirectToAction("Index");

        }

        [HttpGet]
        public ActionResult Dashboard()
        {
            string errMsg = string.Empty;
            if (Session["User"] == null /*&& string.IsNullOrEmpty(Session["User"].ToString())*/)
            {
                return RedirectToAction("Index");
            }
            Products product = new Products();
            ViewBag.Products = product.GetList(null, ref errMsg);
            return View();
        }

        [HttpGet]
        public ActionResult InclusionMaster()
        {
            if (Session["User"] == null /*&& string.IsNullOrEmpty(Session["User"].ToString())*/)
            {
                return RedirectToAction("Index");
            }
            Inclusion Inclusions = new Inclusion();
            ViewBag.Inclusions = Inclusions.GetList();

            return View();
        }

        [HttpPost]
        public ActionResult Inclusions(Inclusion inclusion)
        {
            try
            {
                if (Session["User"] == null /*&& string.IsNullOrEmpty(Session["User"].ToString())*/)
                {
                    return RedirectToAction("Index");
                }
                if (ModelState.IsValid)
                {
                    if (Request.Files.Count > 0)
                    {
                        var dt = DateTime.Now;
                        string getDateTimePart = dt.ToString("yyyyMMdd_hhmmss");

                        var fileAP = Request.Files[0];

                        if (fileAP != null && fileAP.ContentLength > 0)
                        {
                            var extension = Path.GetExtension(fileAP.FileName);
                            var thumbnailPathAfterUpload = "/Uploads/Inclusions/" + inclusion.InclusionName + "/Thumbnail/";

                            if (!Directory.Exists(Server.MapPath("/Uploads/Inclusions/")))
                            {
                                Directory.CreateDirectory(Server.MapPath("/Uploads/Inclusions/" + inclusion.InclusionName + "/Thumbnail/"));
                            }
                            else if (!Directory.Exists(Server.MapPath("/Uploads/Inclusions/" + inclusion.InclusionName)))
                            {
                                Directory.CreateDirectory(Server.MapPath("/Uploads/Inclusions/" + inclusion.InclusionName + "/Thumbnail/"));
                            }
                            else if (!Directory.Exists(Server.MapPath("/Uploads/Inclusions/" + inclusion.InclusionName + "/Thumbnail/")))
                            {
                                Directory.CreateDirectory(Server.MapPath("/Uploads/Inclusions/" + inclusion.InclusionName + "/Thumbnail/"));
                            }

                            var fileName = inclusion.InclusionName + "_Thumbnail" + extension;
                            var path = Path.Combine(Server.MapPath(thumbnailPathAfterUpload), fileName);
                            inclusion.InclusionIconPath = thumbnailPathAfterUpload + fileName;
                            fileAP.SaveAs(path);
                        }
                    }

                    TJ_InclusionMaster newInclusion = new TJ_InclusionMaster
                    {
                        ID = 0,
                        InclusionName = inclusion.InclusionName,
                        InclusionIconPath = inclusion.InclusionIconPath
                    };
                    db.TJ_InclusionMaster.Add(newInclusion);
                    db.SaveChanges();
                    TempData["message"] = "Inclusion saved successfully";
                }
            }
            catch (Exception ex)
            {
                TempData["message"] = ex.Message;
            }
            return RedirectToAction("InclusionMaster");
        }

        [HttpGet]
        public ActionResult ExclusionMaster()
        {
            if (Session["User"] == null /*&& string.IsNullOrEmpty(Session["User"].ToString())*/)
            {
                return RedirectToAction("Index");
            }
            Exclusion Exclusions = new Exclusion();
            ViewBag.Exclusions = Exclusions.GetList();

            return View();
        }

        [HttpPost]
        public ActionResult Exclusions(Exclusion exclusion)
        {
            try
            {
                if (Session["User"] == null /*&& string.IsNullOrEmpty(Session["User"].ToString())*/)
                {
                    return RedirectToAction("Index");
                }
                if (ModelState.IsValid)
                {
                    if (Request.Files.Count > 0)
                    {
                        var dt = DateTime.Now;
                        string getDateTimePart = dt.ToString("yyyyMMdd_hhmmss");

                        var fileAP = Request.Files[0];

                        if (fileAP != null && fileAP.ContentLength > 0)
                        {
                            var extension = Path.GetExtension(fileAP.FileName);
                            var thumbnailPathAfterUpload = "/Uploads/Exclusions/" + exclusion.ExclusionName + "/Thumbnail/";

                            if (!Directory.Exists(Server.MapPath("/Uploads/Exclusions/")))
                            {
                                Directory.CreateDirectory(Server.MapPath("/Uploads/Exclusions/" + exclusion.ExclusionName + "/Thumbnail/"));
                            }
                            else if (!Directory.Exists(Server.MapPath("/Uploads/Exclusions/" + exclusion.ExclusionName)))
                            {
                                Directory.CreateDirectory(Server.MapPath("/Uploads/Exclusions/" + exclusion.ExclusionName + "/Thumbnail/"));
                            }
                            else if (!Directory.Exists(Server.MapPath("/Uploads/Exclusions/" + exclusion.ExclusionName + "/Thumbnail/")))
                            {
                                Directory.CreateDirectory(Server.MapPath("/Uploads/Exclusions/" + exclusion.ExclusionName + "/Thumbnail/"));
                            }

                            var fileName = exclusion.ExclusionName + "_Thumbnail" + extension;
                            var path = Path.Combine(Server.MapPath(thumbnailPathAfterUpload), fileName);
                            exclusion.ExclusionIconPath = thumbnailPathAfterUpload + fileName;
                            fileAP.SaveAs(path);
                        }
                    }

                    TJ_ExclusionMaster newExclusion = new TJ_ExclusionMaster
                    {
                        ID = 0,
                        ExclusionName = exclusion.ExclusionName,
                        ExclusionIconPath = exclusion.ExclusionIconPath
                    };
                    db.TJ_ExclusionMaster.Add(newExclusion);
                    db.SaveChanges();
                    TempData["message"] = "Exclusion saved successfully";
                }
            }
            catch (Exception ex)
            {
                TempData["message"] = ex.Message;
            }
            return RedirectToAction("ExclusionMaster");
        }

        [HttpGet]
        public ActionResult Iternary()
        {
            if (Session["User"] == null /*&& string.IsNullOrEmpty(Session["User"].ToString())*/)
            {
                return RedirectToAction("Index");
            }
            Region region = new Region();
            ViewBag.Regions = region.Get();
            Category category = new Category();
            ViewBag.Categories = category.GetSelectList();
            ViewBag.Status = Iternaries.GetStatus();
            Iternaries iternary = new Iternaries();
            ViewBag.Itenaries = iternary.Get();

            return View();
        }

        [HttpPost]
        public ActionResult Iternary(Iternaries iternary)
        {
            try
            {
                if (Session["User"] == null /*&& string.IsNullOrEmpty(Session["User"].ToString())*/)
                {
                    return RedirectToAction("Index");
                }
                if (ModelState.IsValid)
                {

                    TJ_Iternary newIernary = new TJ_Iternary
                    {
                        Id = 0,
                        CategoryId = Convert.ToInt64(iternary.SelectedCategory),
                        IternaryCode = iternary.IternaryCode,
                        RegionId = Convert.ToInt64(iternary.SelectedRegion),
                        Description = iternary.Description,
                        Status = Convert.ToBoolean(iternary.SelectedStatus)
                    };

                    db.TJ_Iternary.Add(newIernary);
                    db.SaveChanges();
                }
            }
            catch { }
            return RedirectToAction("Iternary");
        }

        [HttpGet]
        public ActionResult Products()
        {
            string errMsg = string.Empty;
            if (Session["User"] == null /*&& string.IsNullOrEmpty(Session["User"].ToString())*/)
            {
                return RedirectToAction("Index");
            }
            Region region = new Region();
            ViewBag.Regions = region.Get();

            Iternaries iternary = new Iternaries();
            ViewBag.Itenaries = iternary.SelectList();

            ViewBag.IternaryStatus = Iternaries.ItenaryStatusValue.GetItenaryStatus();
            ViewBag.TopPromotions = ProductTopPromotionValue.GetTopPromtionStatus();

            Category category = new Category();
            ViewBag.Categories = category.GetSelectList();

            Products product = new Products();
            ViewBag.Products = product.GetList(null, ref errMsg);

            Seasons seasons = new Seasons();
            ViewBag.Seasons = seasons.Get();

            ViewBag.BusinessTypes = Types.GetBusinessTypes();

            Country c = new Models.Country();
            ViewBag.Countries = c.Get();

            Inclusion inclusion = new Inclusion();
            Exclusion exclusion = new Exclusion();
            Products pro = new Models.Products
            {
                Categories = category.GetCheckBoxList(),
                Inclusions = inclusion.GetCheckBoxList(),
                Exclusions = exclusion.GetCheckBoxList()
            };

            return View(pro);
        }

        [HttpPost]
        public ActionResult Products(Products product)
        {
            try
            {
                if (Session["User"] == null /*&& string.IsNullOrEmpty(Session["User"].ToString())*/)
                {
                    return RedirectToAction("Index");
                }
                if (ModelState.IsValid)
                {
                    if (Request.Files.Count > 0)
                    {
                        var dt = DateTime.Now;
                        string getDateTimePart = dt.ToString("yyyyMMdd_hhmmss");

                        var fileAP = Request.Files[0];

                        if (fileAP != null && fileAP.ContentLength > 0)
                        {
                            var extension = Path.GetExtension(fileAP.FileName);
                            var thumbnailPathAfterUpload = "/Uploads/Packages/" + product.ProductIternaryCode + "/Thumbnail/";

                            if (!Directory.Exists(Server.MapPath("/Uploads/Packages/")))
                            {
                                Directory.CreateDirectory(Server.MapPath("/Uploads/Packages/" + product.ProductIternaryCode + "/Thumbnail/"));
                            }
                            else if (!Directory.Exists(Server.MapPath("/Uploads/Packages/" + product.ProductIternaryCode)))
                            {
                                Directory.CreateDirectory(Server.MapPath("/Uploads/Packages/" + product.ProductIternaryCode + "/Thumbnail/"));
                            }
                            else if (!Directory.Exists(Server.MapPath("/Uploads/Packages/" + product.ProductIternaryCode + "/Thumbnail/")))
                            {
                                Directory.CreateDirectory(Server.MapPath("/Uploads/Packages/" + product.ProductIternaryCode + "/Thumbnail/"));
                            }

                            var fileName = "Thumbnail_" + product.ProductIternaryCode + "_" + getDateTimePart + extension;
                            var path = Path.Combine(Server.MapPath(thumbnailPathAfterUpload), fileName);
                            product.Thumbnail = thumbnailPathAfterUpload + fileName;
                            fileAP.SaveAs(path);
                        }
                    }

                    TJ_Products newProduct = new TJ_Products
                    {
                        Id = 0,
                        BusinessType = product.BusinessType,
                        //IternaryCategory = Convert.ToInt64(product.IternaryCategory),
                        IternaryDescription = product.IternaryDescription,
                        //IternaryId = Convert.ToInt64(product.SelectedIternary),
                        NoOfDays = Convert.ToInt16(product.NoOfDays),
                        IternaryStatus = product.IternaryStatus,
                        ProductIternaryCode = product.ProductIternaryCode,
                        NoOfNights = Convert.ToInt16(product.NoOfNights),
                        //RegionId = Convert.ToInt64(product.SelectedRegion),
                        Remark = product.Remark,
                        SeasonId = Convert.ToInt64(product.SelectedSeason),
                        //ValidFrom = Convert.ToDateTime(product.ValidFrom),
                        //ValidTo = Convert.ToDateTime(product.ValidTo)
                        Source = product.Source,
                        Destination = product.Destination,
                        Thumbnail = product.Thumbnail,
                        LocationPath = product.LocationPath,
                        TopPromotion = product.TopPromotion,
                        ProductName = product.ProductName,
                        CountryId = Convert.ToInt64(product.SelectedCountry),
                    };
                    db.TJ_Products.Add(newProduct);
                    db.SaveChanges();
                    TJ_Product_Prices price = new TJ_Product_Prices
                    {
                        CostPrice = product.CostPrice,
                        Discount = product.Discount,
                        ProductId = newProduct.Id,
                        DiscountedPrice = product.CostPrice - (product.CostPrice * (product.Discount / 100)),/* product.DiscountedPrice,*/
                        Id = 0
                    };
                    db.TJ_Product_Prices.Add(price);
                    db.SaveChanges();

                    product.Categories.ToList().ForEach(cat =>
                    {
                        TJ_Product_Categories category = null;
                        //var categoryData = db.TJ_Category.Where(x => x.Name == cat.Text).FirstOrDefault();
                        category = new TJ_Product_Categories
                        {
                            Id = 0,
                            Name = cat.Text,
                            ProductId = newProduct.Id,
                            CategoryId = cat.Id,
                            Checked = cat.Checked
                        };
                        db.TJ_Product_Categories.Add(category);
                    });

                    product.Inclusions.ToList().ForEach(inclusion =>
                    {
                        TJ_Product_Inclusions productInclusion = null;
                        //var inclusionData = db.TJ_InclusionMaster.Where(x => x.InclusionName == inclusion.Text).FirstOrDefault();
                        productInclusion = new TJ_Product_Inclusions
                        {
                            Id = 0,
                            Name = inclusion.Text,
                            ProductId = newProduct.Id,
                            InclusionId = inclusion.Id,
                            Checked = inclusion.Checked
                        };
                        db.TJ_Product_Inclusions.Add(productInclusion);

                    });

                    product.Exclusions.ToList().ForEach(exclusions =>
                    {
                        TJ_Product_Exclusions productExclusion = null;
                       // var inclusionData = db.TJ_ExclusionMaster.Where(x => x.ExclusionName == exclusions.Text).FirstOrDefault();
                        productExclusion = new TJ_Product_Exclusions
                        {
                            Id = 0,
                            Name = exclusions.Text,
                            ProductId = newProduct.Id,
                            InclusionId = exclusions.Id,
                            Checked = exclusions.Checked
                        };
                        db.TJ_Product_Exclusions.Add(productExclusion);
                    });

                    db.SaveChanges();

                    TempData["message"] = "Package saved successfully";
                }
            }
            catch (Exception ex)
            {
                TempData["message"] = ex.Message;
            }
            return RedirectToAction("Products");
        }

        [HttpGet]
        public ActionResult ProductDayWisePlan()
        {
            if (Session["User"] == null /*&& string.IsNullOrEmpty(Session["User"].ToString())*/)
            {
                return RedirectToAction("Index");
            }
            ProductRoutes pr = new ProductRoutes();
            ViewBag.Products = pr.Get();
            ViewBag.DayWisePlans = pr.GetList();
            return View();
        }

        [HttpPost]
        public ActionResult ProductDayWisePlan(List<ProductRoutes> productRoutes)
        {
            try
            {
                if (Session["User"] == null /*&& string.IsNullOrEmpty(Session["User"].ToString())*/)
                {
                    return RedirectToAction("Index");
                }
                bool status = false;
                if (!string.IsNullOrEmpty(productRoutes[0].SelectedProduct))
                {
                    long ProductId = Convert.ToInt64(productRoutes[0].SelectedProduct);
                    var daysLit = db.TJ_Product_Routes.Where(x => x.ProductId == ProductId).Select(x => x.Day ?? 0).ToList();
                    int days = daysLit.Count() > 0 ? daysLit.Max(x => x) : 0;

                    foreach (var item in productRoutes)
                    {

                        if (!string.IsNullOrEmpty(item.City))
                        {
                            days++;
                            TJ_Product_Routes route = null;
                            route = new TJ_Product_Routes
                            {
                                City = item.City,
                                DropPoint = item.DropPoint,
                                PickupPoint = item.PickupPoint,
                                ProductId = ProductId,
                                Description = item.Description,
                                Day = days
                            };
                            db.TJ_Product_Routes.Add(route);
                            db.SaveChanges();
                            status = true;
                        }
                    }

                }
                else
                {
                    TempData["message"] = "Select product code";
                }
                if (status)
                {
                    TempData["message"] = "Data saved successfully...";
                }
            }
            catch (Exception ex) { TempData["message"] = ex.Message; }
            return RedirectToAction("ProductDayWisePlan");
        }

        [HttpGet]
        public ActionResult ProductPhotoGallary()
        {
            if (Session["User"] == null /*&& string.IsNullOrEmpty(Session["User"].ToString())*/)
            {
                return RedirectToAction("Index");
            }
            ProductRoutes pr = new ProductRoutes();
            ViewBag.Products = pr.Get();
            return View();
        }

        [HttpPost]
        public ActionResult ProductPhotoGallary(Products product)
        {
            try
            {
                if (Session["User"] == null /*&& string.IsNullOrEmpty(Session["User"].ToString())*/)
                {
                    return RedirectToAction("Index");
                }
                long productId = Convert.ToInt64(product.SelectedProduct);

                var productCode = db.TJ_Products.Where(x => x.Id == productId).Select(x => x.ProductIternaryCode).FirstOrDefault();

                var photoGallary = db.TJ_PhotoGallary.Where(x => x.ProductId == productId).FirstOrDefault();

                int totalImages = photoGallary != null ? photoGallary.NoOfImages ?? 0 : 0;

                string folderPath = string.Empty;

                if (ModelState.IsValid)
                {
                    if (Request.Files.Count > 0)
                    {
                        var files = Request.Files;
                        int fileLength = Request.Files.Count;
                        for (int i = 0; i < fileLength; i++)
                        {
                            var fileAP = Request.Files[i];
                            if (fileAP != null && fileAP.ContentLength > 0)
                            {
                                var extension = Path.GetExtension(fileAP.FileName);
                                var thumbnailPathAfterUpload = "/Uploads/Packages/" + productCode + "/Gallary/";

                                if (!Directory.Exists(Server.MapPath("/Uploads/Packages/")))
                                {
                                    Directory.CreateDirectory(Server.MapPath("/Uploads/Packages/" + productCode + "/Gallary/"));
                                }
                                else if (!Directory.Exists(Server.MapPath("/Uploads/Packages/" + productCode)))
                                {
                                    Directory.CreateDirectory(Server.MapPath("/Uploads/Packages/" + productCode + "/Gallary/"));
                                }
                                else if (!Directory.Exists(Server.MapPath("/Uploads/Packages/" + productCode + "/Gallary/")))
                                {
                                    Directory.CreateDirectory(Server.MapPath("/Uploads/Packages/" + productCode + "/Gallary/"));
                                }

                                var fileName = "Photo" + "_" + (++totalImages) + extension;
                                var path = Path.Combine(Server.MapPath(thumbnailPathAfterUpload), fileName);
                                folderPath = thumbnailPathAfterUpload;
                                fileAP.SaveAs(path);

                            }
                        }
                    }

                    if (photoGallary == null)
                    {
                        TJ_PhotoGallary photo = new TJ_PhotoGallary
                        {
                            Id = 0,
                            NoOfImages = totalImages,
                            Path = folderPath,
                            ProductId = productId
                        };
                        db.TJ_PhotoGallary.Add(photo);
                    }
                    else
                    {
                        photoGallary.NoOfImages = totalImages;
                    }

                    db.SaveChanges();
                    TempData["message"] = "Photos Uploaded successfully";
                }
            }
            catch (Exception ex)
            {
                TempData["message"] = ex.Message;
            }
            return RedirectToAction("ProductPhotoGallary");
        }

        [HttpGet]
        public ActionResult CategoryMaster()
        {
            if (Session["User"] == null /*&& string.IsNullOrEmpty(Session["User"].ToString())*/)
            {
                return RedirectToAction("Index");
            }
            Category category = new Category();
            ViewBag.category = category.GetList();
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Category(Category category)
        {
            try
            {
                if (Session["User"] == null /*&& string.IsNullOrEmpty(Session["User"].ToString())*/)
                {
                    return RedirectToAction("Index");
                }
                if (ModelState.IsValid)
                {

                    TJ_Category newCategory = new TJ_Category
                    {
                        Id = 0,
                        Name = category.Name
                    };

                    db.TJ_Category.Add(newCategory);
                    db.SaveChanges();
                }
                else
                {
                    Category categoryList = new Category();
                    ViewBag.category = categoryList.GetList();
                    return View("CategoryMaster");
                }
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
            }
            return RedirectToAction("CategoryMaster");
        }

        [HttpGet]
        public ActionResult Region()
        {
            if (Session["User"] == null /*&& string.IsNullOrEmpty(Session["User"].ToString())*/)
            {
                return RedirectToAction("Index");
            }
            Region region = new Region();
            ViewBag.Regions = region.GetList();
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Region(Region region)
        {
            try
            {
                if (Session["User"] == null /*&& string.IsNullOrEmpty(Session["User"].ToString())*/)
                {
                    return RedirectToAction("Index");
                }
                if (ModelState.IsValid)
                {

                    TJ_Region newRegion = new TJ_Region
                    {
                        Id = 0,
                        Code = region.Code,
                        Name = region.Name
                    };

                    db.TJ_Region.Add(newRegion);
                    db.SaveChanges();
                }
                else
                {
                    Region regionList = new Region();
                    ViewBag.Regions = regionList.GetList();

                    return View("Region");

                }
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
                ErrorMessage = ex.Message;
                return View("Region");
            }
            return RedirectToAction("Region");
        }
        [HttpGet]
        public ActionResult CountryMaster()
        {
            if (Session["User"] == null /*&& string.IsNullOrEmpty(Session["User"].ToString())*/)
            {
                return RedirectToAction("Index");
            }
            Region region = new Region();
            ViewBag.Regions = region.Get();
            Country country = new Country();
            ViewBag.Country = country.GetList();
            return View();
        }

        [HttpPost]
        public ActionResult Country(Country country)
        {
            try
            {
                if (Session["User"] == null /*&& string.IsNullOrEmpty(Session["User"].ToString())*/)
                {
                    return RedirectToAction("Index");
                }
                if (ModelState.IsValid)
                {

                    TJ_Country newCountry = new TJ_Country
                    {
                        RegionId = Convert.ToInt64(country.SelectedRegion),
                        CountryCode = country.CountryCode,
                        CountryName = country.CountryName
                    };

                    db.TJ_Country.Add(newCountry);
                    db.SaveChanges();
                }
            }
            catch (Exception ex) { string errMsg = ex.Message; }
            return RedirectToAction("CountryMaster");
        }

        [HttpGet]
        public ActionResult Zone()
        {
            if (Session["User"] == null /*&& string.IsNullOrEmpty(Session["User"].ToString())*/)
            {
                return RedirectToAction("Index");
            }
            Region region = new Region();
            ViewBag.Regions = region.Get();
            Zones zone = new Zones();
            ViewBag.Zones = zone.GetList();
            return View();
        }

        [HttpPost]
        public ActionResult Zone(Zones zone)
        {
            try
            {
                if (Session["User"] == null /*&& string.IsNullOrEmpty(Session["User"].ToString())*/)
                {
                    return RedirectToAction("Index");
                }
                if (ModelState.IsValid)
                {

                    TJ_Zones newZone = new TJ_Zones
                    {
                        Id = 0,
                        RegionId = Convert.ToInt64(zone.SelectedRegion),
                        Code = zone.Code,
                        Name = zone.Name
                    };

                    db.TJ_Zones.Add(newZone);
                    db.SaveChanges();
                }
            }
            catch (Exception ex) { string errMsg = ex.Message; }
            return RedirectToAction("Zone");
        }

        // City controller 
        [HttpGet]
        public ActionResult City()
        {
            if (Session["User"] == null /*&& string.IsNullOrEmpty(Session["User"].ToString())*/)
            {
                return RedirectToAction("Index");
            }
            Zones zone = new Zones();
            ViewBag.Zones = zone.Get();

            Region region = new Region();
            ViewBag.Regions = region.Get();

            Cities city = new Cities();
            ViewBag.Cities = city.GetList();

            return View();
        }
               
        
        [HttpPost]
        public ActionResult City(Cities city)
        {
            try
            {
                if (Session["User"] == null /*&& string.IsNullOrEmpty(Session["User"].ToString())*/)
                {
                    return RedirectToAction("Index");
                }
                if (ModelState.IsValid)
                {

                    TJ_Cities newCity = new TJ_Cities
                    {
                        Id = 0,
                        ZoneId = Convert.ToInt64(city.SelectedZones),
                        Code = city.Code,
                        Name = city.Name
                    };

                    db.TJ_Cities.Add(newCity);
                    db.SaveChanges();
                }
            }
            catch (Exception ex) { string errMsg = ex.Message; }
            return RedirectToAction("City");
        }

        [HttpGet]
        public ActionResult JobCategories()
        {
            if (Session["User"] == null /*&& string.IsNullOrEmpty(Session["User"].ToString())*/)
            {
                return RedirectToAction("Index");
            }
            JobCategory jobCategories = new JobCategory();
            ViewBag.JobCategories = jobCategories.GetList();

            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult JobCategories(JobCategory jobCategories)
        {
            try
            {
                if (Session["User"] == null /*&& string.IsNullOrEmpty(Session["User"].ToString())*/)
                {
                    return RedirectToAction("Index");
                }
                if (ModelState.IsValid)
                {

                    TJ_Job_Category newJobCategory = new TJ_Job_Category
                    {
                        ID = 0,
                        Job_Cat_Name = jobCategories.JobCategoryName
                        

                    };

                    db.TJ_Job_Category.Add(newJobCategory);
                    db.SaveChanges();
                }
                else
                {
                    JobCategory jobCategory = new JobCategory();
                    ViewBag.JobCategories = jobCategory.GetList();

                    return View("JobCategories");
                }
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
                TempData["message"] = ex.Message;
            }
            return RedirectToAction("JobCategories");
        }

        [HttpGet]
        public ActionResult JobVacancies()
        {
            if (Session["User"] == null /*&& string.IsNullOrEmpty(Session["User"].ToString())*/)
            {
                return RedirectToAction("Index");
            }
            JobCategory jobCategory = new JobCategory();
            ViewBag.jobCategory = jobCategory.Get();
            JobVacancies jobVacancies = new JobVacancies();
            ViewBag.JobVacancies = jobVacancies.GetList();

            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult JobVacancies(JobVacancies jobVacancies)
        {
            try
            {
                if (Session["User"] == null /*&& string.IsNullOrEmpty(Session["User"].ToString())*/)
                {
                    return RedirectToAction("Index");
                }
                if (ModelState.IsValid)
                {

                    TJ_JobVacancies newJobVacancy = new TJ_JobVacancies
                    {
                        Id = 0,
                        JobCode = jobVacancies.JobCode,
                        JobTitle = jobVacancies.JobTitle,
                        Job_Category_Id = Convert.ToInt64(jobVacancies.SelectedJobCategory),
                        JobDesignation = jobVacancies.JobDesignation,
                        JobQualification= jobVacancies.Qualification,
                        JobCTC = jobVacancies.JobCTC,
                        JobDescription = jobVacancies.JobDescription,
                        MinExperience = jobVacancies.MinExperience,
                        MaxExperience = jobVacancies.MaxExperience,

                    };

                    db.TJ_JobVacancies.Add(newJobVacancy);
                    db.SaveChanges();
                }
                else
                {
                    JobVacancies jobVacancy = new JobVacancies();
                    ViewBag.JobVacancies = jobVacancy.GetList();

                    return View("JobVacancies");
                }
            }
            catch (Exception ex) {
                string errMsg = ex.Message;
                TempData["message"] = ex.Message;
            }
            return RedirectToAction("JobVacancies");
        }

        
        public ActionResult DeleteProduct(long id, string ViewName)
        {
            try
            {
                var package = db.TJ_Products.Where(x => x.Id == id).FirstOrDefault();
                if (package != null)
                {

                    TempData["message"] = Models.Products.Delete(id);
                }
            }
            catch (Exception ex)
            {
                TempData["message"] = ex.Message;
            }
            return RedirectToAction(ViewName);
        }
        public ActionResult DeleteExclusionMaster(long id)
        {
            try
            {
                var exclusion = db.TJ_ExclusionMaster.Where(x => x.ID == id).FirstOrDefault();
                if (exclusion != null)
                {
                    db.TJ_ExclusionMaster.Remove(exclusion);
                    db.SaveChanges();
                    TempData["message"] = "Exclusion deleted successfully...";
                }
                else
                {
                    TempData["message"] = "Invalid Exclusion!!!";
                }


            }
            catch (Exception ex)
            {
                TempData["message"] = ex.Message;
            }
            return RedirectToAction("ExclusionMaster");
        }
        public ActionResult DeleteInclusionMaster(long id)
        {
            try
            {
                var inclusion = db.TJ_InclusionMaster.Where(x => x.ID == id).FirstOrDefault();
                if (inclusion != null)
                {
                    db.TJ_InclusionMaster.Remove(inclusion);
                    db.SaveChanges();
                    TempData["message"] = "Inclusion deleted successfully...";
                }
                else
                {
                    TempData["message"] = "Invalid Inclusion!!!";
                }


            }
            catch (Exception ex)
            {
                TempData["message"] = ex.Message;
            }
            return RedirectToAction("InclusionMaster");
        }
        public ActionResult DeleteCategoryMaster(long id)
        {
            try
            {
                var category = db.TJ_Category.Where(x => x.Id == id).FirstOrDefault();
                if (category != null)
                {
                    category.TJ_Iternary.ToList().ForEach(x => x.CategoryId = 0);
                    category.TJ_Products.ToList().ForEach(p => p.IternaryCategory = null);
                    category.TJ_Product_Categories.ToList().ForEach(x => x.CategoryId = null);
                    category.TJ_Product_Category.ToList().ForEach(x => x.CategoryId = 0);
                    db.TJ_Category.Remove(category);
                    db.SaveChanges();
                    TempData["message"] = "Category removed successfully...";
                }
                else
                {
                    TempData["message"] = "Invalid Category!!!";
                }


            }
            catch (Exception ex)
            {
                TempData["message"] = ex.Message;
            }
            return RedirectToAction("CategoryMaster");
        }
        public ActionResult DeleteCityMaster(long id)
        {
            try
            {
                var city = db.TJ_Cities.Where(x => x.Id == id).FirstOrDefault();
                if (city != null)
                {
                    city.TJ_Products.ToList().ForEach(p => p.CityId = null);
                    db.TJ_Cities.Remove(city);
                    db.SaveChanges();
                    TempData["message"] = "City deleted successfully...";
                }
                else
                {
                    TempData["message"] = "Invalid City!!!";
                }


            }
            catch (Exception ex)
            {
                TempData["message"] = ex.Message;
            }
            return RedirectToAction("City");
        }
        public ActionResult DeleteCoutryMaster(long id)
        {
            try
            {
                var country = db.TJ_Country.Where(x => x.ID == id).FirstOrDefault();
                if (country != null)
                {
                    country.TJ_Cities.ToList().ForEach(x => x.CountryId = null);
                    country.TJ_Zones.ToList().ForEach(x => x.CountryId = null);
                    db.TJ_Country.Remove(country);
                    db.SaveChanges();
                    TempData["message"] = "Contry removed successfully...";
                }
                else
                {
                    TempData["message"] = "Country is unavailable!!!";
                }


            }
            catch (Exception ex)
            {
                TempData["message"] = ex.Message;
            }
            return RedirectToAction("CountryMaster");
        }
        public ActionResult DeleteZone(long id)
        {
            try
            {
                var zone = db.TJ_Zones.Where(x => x.Id == id).FirstOrDefault();
                if (zone != null)
                {
                    zone.TJ_Cities.ToList().ForEach(x => x.CountryId = null);
                    zone.TJ_Products.ToList().ForEach(x => x.ZoneId = null);
                    db.TJ_Zones.Remove(zone);
                    db.SaveChanges();
                    TempData["message"] = "Zone removed successfully...";
                }
                else
                {
                    TempData["message"] = "Zone is unavailable!!!";
                }


            }
            catch (Exception ex)
            {
                TempData["message"] = ex.Message;
            }
            return RedirectToAction("Zone");
        }

        public ActionResult DeleteRegion(long id)
        {
            try
            {
                var region = db.TJ_Region.Where(x => x.Id == id).FirstOrDefault();
                if (region != null)
                {
                    region.TJ_Cities.ToList().ForEach(x => x.CountryId = null);
                    region.TJ_Products.ToList().ForEach(x => x.ZoneId = null);
                    region.TJ_Country.ToList().ForEach(x => x.RegionId = 0);
                    region.TJ_Iternary.ToList().ForEach(x => x.RegionId = 0);
                    region.TJ_Zones.ToList().ForEach(x => x.RegionId = 0);

                    db.TJ_Region.Remove(region);
                    db.SaveChanges();
                    TempData["message"] = "Region removed successfully...";
                }
                else
                {
                    TempData["message"] = "Region is unavailable!!!";
                }


            }
            catch (Exception ex)
            {
                TempData["message"] = ex.Message;
            }
            return RedirectToAction("Region");
        }

        public ActionResult DeleteJobVacancies(long id)
        {
            try
            {
                var jobVacancy = db.TJ_JobVacancies.Where(x => x.Id == id).FirstOrDefault();
                if (jobVacancy != null)
                {
                    
                    db.TJ_JobVacancies.Remove(jobVacancy);
                    db.SaveChanges();
                    TempData["message"] = "Job Vacancy removed successfully...";
                }
                else
                {
                    TempData["message"] = "Job Vacancy is unavailable!!!";
                }


            }
            catch (Exception ex)
            {
                TempData["message"] = ex.Message;
            }
            return RedirectToAction("JobVacancies");
        }
    }
}