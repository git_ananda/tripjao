﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TripJao.Models;

namespace TripJao.Controllers
{
    public class CareersController : Controller
    {
        TripJaoEntities db = new TripJaoEntities();
        // GET: Careers
        public ActionResult Index(int? page)
        {
            string errMsg = string.Empty;

            Region regions = new Region();
            ViewBag.Regions = regions.GetList();

            Category categories = new Category();
            ViewBag.Categories = categories.GetList();
            ViewBag.CategorySelectList = categories.GetSelectList();

            ViewBag.DurationsSelectList = ProductDuration.GetSelectList();

            JobVacancies jobs = new JobVacancies();
            
            var jobOpenings = jobs.GetList();

            var pager = new Pager(jobOpenings.Count(), page);

            var jobsData = new Pagination
            {
                jobOpenings = jobOpenings.Skip((pager.CurrentPage - 1) * pager.PageSize).Take(pager.PageSize),
                Pager = pager
            };
            return View(jobsData);
            
        }
    }
}