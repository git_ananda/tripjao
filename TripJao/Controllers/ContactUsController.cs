﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TripJao.Models;
using TripJao.Utilities;

namespace TripJao.Controllers
{
    public class ContactUsController : Controller
    {
        TripJaoEntities db = new TripJaoEntities();
        // GET: Enquiry
        public ActionResult Index()
        {
            string errMsg = string.Empty;

            Products products = new Products();
            ViewBag.Products = products.GetList(ProductTypes.Promotion, ref errMsg);

            Region regions = new Region();
            ViewBag.Regions = regions.GetList();

            Category categories = new Category();
            ViewBag.CategorySelectList = categories.GetSelectList();

            ViewBag.DurationsSelectList = ProductDuration.GetSelectList();

            if (!string.IsNullOrEmpty(errMsg))
            {
                TempData["message"] = errMsg;
            }
            return View();
        }

        [HttpPost]
        public ActionResult Add(CustomerFeedback customerFeedback)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string mailStatus = EmailUtility.sendCustomerFeedbackMail(customerFeedback);
                    return Json(new { status = 1, msg = mailStatus });
                }
                return Json(new { status = 0, msg = "You have entered invalid information" });
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, msg = ex.Message });
            }
        }
    }
}