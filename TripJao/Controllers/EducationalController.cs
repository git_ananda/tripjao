﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TripJao.Models;

namespace TripJao.Controllers
{
    public class EducationalController : Controller
    {
        // GET: Educational
        public ActionResult Index(int? page)
        {
            string errMsg = string.Empty;
            Region regions = new Region();
            ViewBag.Regions = regions.GetList();

            Category categories = new Category();
            ViewBag.Categories = categories.GetList();
            ViewBag.CategorySelectList = categories.GetSelectList();

            ViewBag.DurationsSelectList = ProductDuration.GetSelectList();

            Products product = new Products();
            var Products = product.GetList(ProductTypes.EducationalTours, ref errMsg);

            var pager = new Pager(Products.Count(), page);

            var productsData = new Pagination
            {
                Products = Products.Skip((pager.CurrentPage - 1) * pager.PageSize).Take(pager.PageSize),
                Pager = pager
            };
            return View(productsData);
        }

        // GET: Educational/IndustrialVisits
        public ActionResult IndustrialVisits(int? page)
        {
            string errMsg = string.Empty;

            Category categories = new Category();
            ViewBag.Categories = categories.GetList();
            ViewBag.CategorySelectList = categories.GetSelectList();

            ViewBag.DurationsSelectList = ProductDuration.GetSelectList();

            Region regions = new Region();
            ViewBag.Regions = regions.GetList();


            Products product = new Products();
            var Products = product.GetList(ProductTypes.IndustrialTours, ref errMsg);

            var pager = new Pager(Products.Count(), page);

            var productsData = new Pagination
            {
                Products = Products.Skip((pager.CurrentPage - 1) * pager.PageSize).Take(pager.PageSize),
                Pager = pager
            };
            return View(productsData);
        }

        // GET: Educational/Competitions
        public ActionResult Competitions(int? page)
        {
            string errMsg = string.Empty;

            Category categories = new Category();
            ViewBag.Categories = categories.GetList();
            ViewBag.CategorySelectList = categories.GetSelectList();

            ViewBag.DurationsSelectList = ProductDuration.GetSelectList();

            Region regions = new Region();
            ViewBag.Regions = regions.GetList();


            Products product = new Products();
            var Products = product.GetList(ProductTypes.EducationalCompetition, ref errMsg);

            var pager = new Pager(Products.Count(), page);

            var productsData = new Pagination
            {
                Products = Products.Skip((pager.CurrentPage - 1) * pager.PageSize).Take(pager.PageSize),
                Pager = pager
            };
            return View(productsData);
        }

        // GET: Educational/Expos
        public ActionResult Expos(int? page)
        {
            string errMsg = string.Empty;

            Category categories = new Category();
            ViewBag.Categories = categories.GetList();
            ViewBag.CategorySelectList = categories.GetSelectList();

            ViewBag.DurationsSelectList = ProductDuration.GetSelectList();

            Region regions = new Region();
            ViewBag.Regions = regions.GetList();


            Products product = new Products();
            var Products = product.GetList(ProductTypes.ExposSummetsTours, ref errMsg);

            var pager = new Pager(Products.Count(), page);

            var productsData = new Pagination
            {
                Products = Products.Skip((pager.CurrentPage - 1) * pager.PageSize).Take(pager.PageSize),
                Pager = pager
            };
            return View(productsData);
        }

        // GET: Educational/International
        public ActionResult International(int? page)
        {
            string errMsg = string.Empty;

            Category categories = new Category();
            ViewBag.Categories = categories.GetList();
            ViewBag.CategorySelectList = categories.GetSelectList();

            ViewBag.DurationsSelectList = ProductDuration.GetSelectList();

            Region regions = new Region();
            ViewBag.Regions = regions.GetList();


            Products product = new Products();
            var Products = product.GetList(ProductTypes.EducationaInternational, ref errMsg);

            var pager = new Pager(Products.Count(), page);

            var productsData = new Pagination
            {
                Products = Products.Skip((pager.CurrentPage - 1) * pager.PageSize).Take(pager.PageSize),
                Pager = pager
            };
            return View(productsData);
        }

        // GET: Educational/Details/5
        public ActionResult Details(string id)
        {
            Category categories = new Category();
            ViewBag.Categories = categories.GetList();
            ViewBag.CategorySelectList = categories.GetSelectList();

            ViewBag.DurationsSelectList = ProductDuration.GetSelectList();

            Products ProductData = new Products();
            if (!string.IsNullOrEmpty(id))
            {
                long productId = Convert.ToInt64(id);
                Products product = new Products();
                ProductData = product.GetDetails(productId);
                ProductData.isEducationalTour = true;
            }

            Region regions = new Region();
            ViewBag.Regions = regions.GetList();

            PackageEnquiryForm packageInquiry = new PackageEnquiryForm();
            ViewBag.PackageInquiry = packageInquiry;

            return View(ProductData);
        }

        // GET: Educational/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Educational/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Educational/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Educational/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Educational/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Educational/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
