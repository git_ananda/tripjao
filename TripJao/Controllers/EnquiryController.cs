﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TripJao.Models;
using TripJao.Utilities;

namespace TripJao.Controllers
{
    public class EnquiryController : Controller
    {
        TripJaoEntities db = new TripJaoEntities();
        // GET: Enquiry
        public ActionResult Index()
        {
            return View();
        }

       [HttpPost]
        public ActionResult Add(PackageEnquiryForm packageInquiryForm)
        {
            try
            {
                bool status = false;

                TJ_Product_Enquiry newProductEnquiry = null;
                newProductEnquiry = new TJ_Product_Enquiry
                {
                    ID = 0,
                    ContactNumber = packageInquiryForm.ContactNumber,
                    Email = packageInquiryForm.Email,
                    Enquiry = packageInquiryForm.Enquiry,
                    Name = packageInquiryForm.Name,
                    Status = "A",
                    NoOfTravellers = packageInquiryForm.NoOfTravellers,
                    TentativeDate = packageInquiryForm.TentativeDate,
                    TentativeBudget = packageInquiryForm.TentativeBudget,
                    Duration = packageInquiryForm.Duration,
                    NoOfAdults = packageInquiryForm.AdultCount,
                    NoOfChildren = packageInquiryForm.ChildCount,
                    Address = packageInquiryForm.Address,
                    Destination = packageInquiryForm.Destination,
                    TourType = packageInquiryForm.TourType,
                    Department = packageInquiryForm.Department,
                    InstituteName = packageInquiryForm.InstituteName,
                    NoOfStudents = packageInquiryForm.NoOfStudents,
                    NoOfTeachers = packageInquiryForm.NoOfTeachers,
                    ProductId = packageInquiryForm.ProductId                    
                };
               
                db.TJ_Product_Enquiry.Add(newProductEnquiry);
                db.SaveChanges();
                status = true;

                Products product = new Products();
                packageInquiryForm.SelectedProduct = product.GetDetails(packageInquiryForm.ProductId??0);

                if (status)
                {
                    EmailUtility.sendPackageInquiryMail(packageInquiryForm);
                    EmailUtility.sendReplyToPackageEnquiry(packageInquiryForm, Server.MapPath("~/Views/Home/PackageEnquiryReply.html"));
                    return Json(new { status = 1, msg = "Success" });

                }
                else
                {
                    return Json(new { status = 0, msg = "Failed" });
                }
            }
            catch(Exception ex)
            {
                return Json(new { status = 0, msg = ex.Message });
            }
        }
    }
}