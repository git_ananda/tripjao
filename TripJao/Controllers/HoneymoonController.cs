﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TripJao.Models;

namespace TripJao.Controllers
{
    public class HoneymoonController : Controller
    {
        // GET: Honeymoon
        public ActionResult Index()
        {
            return View();
        }

        // GET: Honeymoon/Domastic
        public ActionResult DomesticHoneymoon(int? page)
        {
            
               string errMsg = string.Empty;

                Category categories = new Category();
                ViewBag.Categories = categories.GetList();
                ViewBag.CategorySelectList = categories.GetSelectList();

                ViewBag.DurationsSelectList = ProductDuration.GetSelectList();

                Region regions = new Region();
                ViewBag.Regions = regions.GetList();


                Products product = new Products();
                var Products = product.GetHoneymoonList(ProductTypes.HoneymoonTours,ProductTypes.DomesticTour, ref errMsg);

                var pager = new Pager(Products.Count(), page);

                var productsData = new Pagination
                {
                    Products = Products.Skip((pager.CurrentPage - 1) * pager.PageSize).Take(pager.PageSize),
                    Pager = pager
                };
                return View(productsData);
        }

        // GET: Honeymoon/International
        public ActionResult InternationalHoneymoon(int? page)
        {
            string errMsg = string.Empty;

            Category categories = new Category();
            ViewBag.Categories = categories.GetList();
            ViewBag.CategorySelectList = categories.GetSelectList();

            ViewBag.DurationsSelectList = ProductDuration.GetSelectList();

            Region regions = new Region();
            ViewBag.Regions = regions.GetList();


            Products product = new Products();
            var Products = product.GetHoneymoonList(ProductTypes.HoneymoonTours, ProductTypes.InternationalTour, ref errMsg);

            var pager = new Pager(Products.Count(), page);

            var productsData = new Pagination
            {
                Products = Products.Skip((pager.CurrentPage - 1) * pager.PageSize).Take(pager.PageSize),
                Pager = pager
            };
            return View(productsData);
        }
    }
}