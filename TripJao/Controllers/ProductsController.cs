﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using TripJao.Models;
using TripJao.Utilities;

namespace TripJao.Controllers
{
    public class ProductsController : Controller
    {
        TripJaoEntities db = new TripJaoEntities();
        // GET: Package
        public ActionResult Index(Package package)
        {
            string errMsg = string.Empty;

            IEnumerable<TJ_Products> productQuery = db.TJ_Products;

            if(package.CategoryId > 0)
            {
                productQuery = from pro in productQuery
                               join pro_cat in db.TJ_Product_Categories on pro.Id equals pro_cat.ProductId
                                where pro_cat.CategoryId == package.CategoryId
                                select pro;
            }

            if(!string.IsNullOrEmpty(package.Destination))
            {
                productQuery = productQuery.Where(x => x.Destination.ToLower().Contains(package.Destination.ToLower()));
            }

            Products pr = new Products();

            List<Products> products = productQuery.AsEnumerable().Select(x => new Products
            {
                Id = x.Id,
                IternaryDescription = x.GetDescription(),
                NoOfDays = x.NoOfDays,
                NoOfNights = x.NoOfNights,
                Destination = x.Destination,
                CostPrice = x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().CostPrice : 0,
                Discount = x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().Discount : 0,
                DiscountedPrice = x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().DiscountedPrice : 0,
                ThumbnailPath = pr.GetThumbnailPath(x.ProductIternaryCode)
            }).OrderByDescending(x => x.Id).ToList();

           // package.Products = products;            
            
            Category categories = new Category();
            package.Categories = categories.GetSelectList();
            package.Categories.ToList().ForEach(item =>
            {
                if(Convert.ToInt64(item.Value) == package.CategoryId)
                {
                    item.Selected = true;
                }
            });
           
            package.Durations = ProductDuration.GetSelectList();
            package.Durations.ToList().ForEach(item =>
            {
                if (Convert.ToInt64(item.Value) == package.DurationId)
                {
                    item.Selected = true;
                }
            });

            var pager = new Pager(products.Count(), package.Page);

            var productsData = new Pagination
            {
                Package = package,
                Products = products.Skip((pager.CurrentPage - 1) * pager.PageSize).Take(pager.PageSize).ToList(),
                Pager = pager
            };

            ViewBag.CategorySelectList = categories.GetSelectList();

            ViewBag.DurationsSelectList = ProductDuration.GetSelectList();

            return View(productsData);
        }
                
        public ActionResult Details(string id)
        {
            Category categories = new Category();
            ViewBag.Categories = categories.GetList();
            ViewBag.CategorySelectList = categories.GetSelectList();

            ViewBag.DurationsSelectList = ProductDuration.GetSelectList();

            Products ProductData = new Products();            
            if (!string.IsNullOrEmpty(id))
            {
                long productId = Convert.ToInt64(id);
                Products product = new Products();
                ProductData = product.GetDetails(productId);
            }

            Region regions = new Region();
            ViewBag.Regions = regions.GetList();

            PackageEnquiryForm packageInquiry = new PackageEnquiryForm();
            ViewBag.PackageInquiry = packageInquiry;
           
            return View(ProductData);
        }

        [HttpPost]
        public ActionResult customerInquiry(PackageEnquiryForm custInquiry)
        {
            String emailMessage = "";

            emailMessage = EmailUtility.sendPackageInquiryMail(custInquiry);

            return RedirectToAction("Details");
        }
    }
}