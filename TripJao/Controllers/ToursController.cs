﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TripJao.Models;

namespace TripJao.Controllers
{
    public class ToursController : Controller
    {
        // GET: Tours
        public ActionResult Index()
        {
            string errMsg = string.Empty;

            Products products = new Products();
            ViewBag.Products = products.GetList(ProductTypes.Promotion, ref errMsg);

            Region regions = new Region();
            ViewBag.Regions = regions.GetList();

            Category categories = new Category();
            ViewBag.CategorySelectList = categories.GetSelectList();

            ViewBag.DurationsSelectList = ProductDuration.GetSelectList();

            if (!string.IsNullOrEmpty(errMsg))
            {
                TempData["message"] = errMsg;
            }
            return View();
        }

        public ActionResult ToursMaster()
        {
            Category categories = new Category();
            ViewBag.Categories = categories.GetList();

            ViewBag.CategorySelectList = categories.GetSelectList();

            ViewBag.DurationsSelectList = ProductDuration.GetSelectList();

            Region regions = new Region();
            ViewBag.Regions = regions.GetList();

            return View();
        }

        public ActionResult About()
        {

            Category categories = new Category();
            ViewBag.Categories = categories.GetList();
            ViewBag.CategorySelectList = categories.GetSelectList();

            ViewBag.DurationsSelectList = ProductDuration.GetSelectList();

            Region regions = new Region();
            ViewBag.Regions = regions.GetList();

            ViewBag.Message = "Your application description page.";
            return View();
        }

        public ActionResult Contact()
        {

            Category categories = new Category();
            ViewBag.Categories = categories.GetList();
            ViewBag.CategorySelectList = categories.GetSelectList();

            ViewBag.DurationsSelectList = ProductDuration.GetSelectList();

            Region regions = new Region();
            ViewBag.Regions = regions.GetList();

            ViewBag.Message = "Your contact page.";
            return View();
        }

        public ActionResult HoneyMoonTours()
        {

            Category categories = new Category();
            ViewBag.CategorySelectList = categories.GetSelectList();

            ViewBag.DurationsSelectList = ProductDuration.GetSelectList();

            Region regions = new Region();
            ViewBag.Regions = regions.GetList();

            ViewBag.Categories = categories.GetList();

            return View();
        }

        public ActionResult EducationalTours()
        {
            string errMsg = string.Empty;

            Category categories = new Category();
            ViewBag.CategorySelectList = categories.GetSelectList();

            ViewBag.DurationsSelectList = ProductDuration.GetSelectList();

            Region regions = new Region();
            ViewBag.Regions = regions.GetList();

            ViewBag.Categories = categories.GetList();

            Products products = new Products();
            ViewBag.Products = products.getEducationalTopTours(ProductTypes.EducationalTours, ref errMsg);
            //products.GetList(ProductTypes.Promotion, ref errMsg);

            return View();
        }

        public ActionResult TourPackageDetail()
        {

            Category categories = new Category();
            ViewBag.Categories = categories.GetList();

            ViewBag.CategorySelectList = categories.GetSelectList();

            ViewBag.DurationsSelectList = ProductDuration.GetSelectList();

            Region regions = new Region();
            ViewBag.Regions = regions.GetList();

            return View();
        }


        public ActionResult FarByCar(int? page)
        {
            string errMsg = string.Empty;
            Region regions = new Region();
            ViewBag.Regions = regions.GetList();

            Category categories = new Category();
            ViewBag.Categories = categories.GetList();
            ViewBag.CategorySelectList = categories.GetSelectList();

            ViewBag.DurationsSelectList = ProductDuration.GetSelectList();

            ViewBag.IsPageLoadFirstTime = page == null ? true : false;

            Products product = new Products();
            var Products = product.GetList(ProductTypes.FarByCar, ref errMsg);

            var pager = new Pager(Products.Count(), page);

            var productsData = new Pagination
            {
                Products = Products.Skip((pager.CurrentPage - 1) * pager.PageSize).Take(pager.PageSize),
                Pager = pager
            };

            return View(productsData);
        }

        public ActionResult DomesticTours(int? page)
        {
            string errMsg = string.Empty;
            Region regions = new Region();
            Category categories = new Category();
            ViewBag.Regions = regions.GetList();
            ViewBag.Categories = categories.GetList();

            ViewBag.CategorySelectList = categories.GetSelectList();
            ViewBag.DurationsSelectList = ProductDuration.GetSelectList();

            Products product = new Products();
            var Products = product.GetList(ProductTypes.DomesticTour, ref errMsg);

            var pager = new Pager(Products.Count(), page);

            var productsData = new Pagination
            {
                Products = Products.Skip((pager.CurrentPage - 1) * pager.PageSize).Take(pager.PageSize),
                Pager = pager
            };

            return View(productsData);
        }

        public ActionResult InternationalTours(int? page)
        {
            string errMsg = string.Empty;

            Category categories = new Category();
            ViewBag.Categories = categories.GetList();
            ViewBag.CategorySelectList = categories.GetSelectList();

            ViewBag.DurationsSelectList = ProductDuration.GetSelectList();

            Region regions = new Region();
            ViewBag.Regions = regions.GetList();


            Products product = new Products();
            var Products = product.GetList(ProductTypes.InternationalTour, ref errMsg);

            var pager = new Pager(Products.Count(), page);

            var productsData = new Pagination
            {
                Products = Products.Skip((pager.CurrentPage - 1) * pager.PageSize).Take(pager.PageSize),
                Pager = pager
            };

            return View(productsData);
        }

        public ActionResult CorporateTours(int? page)
        {
            string errMsg = string.Empty;
            Region regions = new Region();
            Category categories = new Category();
            ViewBag.Regions = regions.GetList();
            ViewBag.Categories = categories.GetList();

            ViewBag.CategorySelectList = categories.GetSelectList();
            ViewBag.DurationsSelectList = ProductDuration.GetSelectList();

            Products product = new Products();
            var Products = product.GetList(ProductTypes.CorporateTour, ref errMsg);

            var pager = new Pager(Products.Count(), page);

            var productsData = new Pagination
            {
                Products = Products.Skip((pager.CurrentPage - 1) * pager.PageSize).Take(pager.PageSize),
                Pager = pager
            };

            return View(productsData);
        }

        public ActionResult CulturalTours(int? page)
        {
            string errMsg = string.Empty;
            Region regions = new Region();
            Category categories = new Category();
            ViewBag.Regions = regions.GetList();
            ViewBag.Categories = categories.GetList();

            ViewBag.CategorySelectList = categories.GetSelectList();
            ViewBag.DurationsSelectList = ProductDuration.GetSelectList();

            Products product = new Products();
            var Products = product.GetList(ProductTypes.CorporateTour, ref errMsg);

            var pager = new Pager(Products.Count(), page);

            var productsData = new Pagination
            {
                Products = Products.Skip((pager.CurrentPage - 1) * pager.PageSize).Take(pager.PageSize),
                Pager = pager
            };

            return View(productsData);
        }

        public ActionResult BlogPost()
        {

            Category categories = new Category();
            ViewBag.Categories = categories.GetList();

            ViewBag.CategorySelectList = categories.GetSelectList();

            ViewBag.DurationsSelectList = ProductDuration.GetSelectList();

            Region regions = new Region();
            ViewBag.Regions = regions.GetList();

            return View();
        }

        public ActionResult BlogDetail()
        {
            Category categories = new Category();
            ViewBag.Categories = categories.GetList();

            ViewBag.CategorySelectList = categories.GetSelectList();

            ViewBag.DurationsSelectList = ProductDuration.GetSelectList();

            Region regions = new Region();
            ViewBag.Regions = regions.GetList();

            return View();

        }
        public ActionResult FAQ()
        {
            Category categories = new Category();
            ViewBag.Categories = categories.GetList();

            ViewBag.CategorySelectList = categories.GetSelectList();

            ViewBag.DurationsSelectList = ProductDuration.GetSelectList();

            Region regions = new Region();
            ViewBag.Regions = regions.GetList();

            return View();

        }

        public ActionResult TermsAndConditions()
        {
            Inclusion inclusions = new Inclusion();
            ViewBag.Inclusions = inclusions.GetList();

            Exclusion exclusions = new Exclusion();
            ViewBag.Exclusions = exclusions.GetList();

            return View();
        }

        public ActionResult Gallery(int? page)
        {
            Category categories = new Category();
            ViewBag.Categories = categories.GetList();

            ViewBag.CategorySelectList = categories.GetSelectList();

            ViewBag.DurationsSelectList = ProductDuration.GetSelectList();

            Region regions = new Region();
            ViewBag.Regions = regions.GetList();

            Products product = new Products();
            var products = product.GetAllProductWiseGalleryPaths();

            var pager = new Pager(products.Count(), page);

            var productWiseGallery = new Pagination
            {
                Products = products.Skip((pager.CurrentPage - 1) * pager.PageSize).Take(pager.PageSize),
                Pager = pager
            };
            return View(productWiseGallery);
        }
    }
}