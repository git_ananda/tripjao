﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TripJao.Models;
using TripJao.Utilities;

namespace TripJao.Controllers
{
    public class UserController : Controller
    {
        TripJaoEntities db = new TripJaoEntities();
        // GET: User
        public ActionResult Index()
        {
            if (Session["User"] == null /*&& string.IsNullOrEmpty(Session["User"].ToString())*/)
            {
                return RedirectToAction("Index","Admin");
            }
            List<TJ_Users> users = db.TJ_Users.ToList();
            return View(users);
        }

        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(Users user)
        {
            if (Session["User"] == null /*&& string.IsNullOrEmpty(Session["User"].ToString())*/)
            {
                return RedirectToAction("Index", "Admin");
            }
            string errMsg = "";
            try
            {
                int status = 0;
                IEnumerable<TJ_Users> usersQuery = db.TJ_Users;
                var chkUser = db.TJ_Users.Where(x => x.UserName.Equals(user.UserName));

                if (chkUser.Count()<=0)
                {
                    var keyNew = PasswordEncDncUtility.GeneratePassword(10);
                    var password = PasswordEncDncUtility.EncodePassword(user.Password, keyNew);
                    user.Password = password;
                    
                    TJ_Users newUser = new TJ_Users
                    {
                        FirstName = user.FirstName,
                        Id = 0,
                        Password = user.Password,
                        EncryptionKey= keyNew,
                        LastName = user.LastName,
                        Type = user.Type,
                        Status = user.Status,
                        UserName = user.UserName,
                        createdAt = DateTime.Now,
                        UpdatedAt = DateTime.Now,
                    };

                    db.TJ_Users.Add(newUser);
                    status = db.SaveChanges();
                    if (status == 1)
                    {
                        return RedirectToAction("Index", "Admin");
                    }
                }
                errMsg = "User Allredy Exixts!!";
                return RedirectToAction("Index", "User");
            }
            catch(Exception ex)
            {
                errMsg = ex.Message;
                return RedirectToAction("Index", "User");
            }           
        }
    }
}