﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TripJao.Models;

namespace TripJao.Models
{
    public class Category
    {
        public long Id { get; set; }

        [DisplayName("Category Name")]
        [Required(ErrorMessage = "Category Name is Required", AllowEmptyStrings = false)]
        [RegularExpression(@"^[a-zA-Z &][a-zA-Z &]+$",
                            ErrorMessage = "Category Name is not valid")]
        public string Name { get; set; }       
        TripJaoEntities db = null;
        public Category()
        {
            db = new TripJaoEntities();
        }
        public List<SelectListItem> GetSelectList()
        {
            try
            {
                List<SelectListItem> categories = new List<SelectListItem>();
                var categoryList = db.TJ_Category.Select(x => new { x.Id, x.Name}).ToList();
                foreach (var item in categoryList)
                {
                    categories.Add(new SelectListItem { Text = item.Name, Value = item.Id.ToString() });
                }
                return categories;
            }
            catch(Exception ex)
            {
                string errMsg = ex.Message;
                return new List<SelectListItem>();
            }
        }

        public List<CheckBoxes> GetCheckBoxList()
        {
            try
            {
                List<CheckBoxes> categories = new List<CheckBoxes>();
                var categoryList = db.TJ_Category.Select(x => new { x.Id, x.Name,x.CategoryCode }).ToList();
                foreach (var item in categoryList)
                {
                    categories.Add(new CheckBoxes { Text = item.Name, Checked = item.Id == 1 ? true : false, Id = item.Id  });
                }
                return categories;
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
                return new List<CheckBoxes>();
            }
        }

        public List<Category> GetList()
        {
            try
            {
                List<Category> data = db.TJ_Category.Select(x => new Category
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList();
                return data;
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
                return new List<Category>();
            }
        }
    }

    public class CheckBoxes
    {
        public long Id { get; set; }
        public string Text { get; set; }
        public bool Checked { get; set; }
    }
}