﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace TripJao.Models
{
    public class Cities
    {

        public long Id { get; set; }
        public long RegionId { get; set; }

        [DisplayName("Region")]
        public string SelectedRegion { get; set; }
        public long ZoneId { get; set; }

        [DisplayName("Zone")]
        public string SelectedZones { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }

        TripJaoEntities db = null;
        
        public Cities()
        {
            db = new TripJaoEntities();
        }

        public List<Cities> GetList()
        {
            try
            {
                List<Cities> data = db.TJ_Cities.Select(x => new Cities
                {
                    Id = x.Id,
                    Name = x.Name,
                    Code = x.Code,
                    SelectedZones = x.TJ_Zones != null ? x.TJ_Zones.Name : "",
                   SelectedRegion = x.TJ_Region != null ? x.TJ_Region.Name : "",
                }).ToList();
                return data;
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
                return new List<Cities>();
            }

        }
    }
}