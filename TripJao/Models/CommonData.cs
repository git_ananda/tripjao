﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TripJao.Models
{
    public class CommonData
    {
        public static List<string> GetAdminControllerList()
        {
            return new List<string>()
            {
                "admin",
                "user",
                "product",
                "product_iternaries",
                "region",
                "country",
                "zones",
                "cities",
                "category",
                "jobvacancies",
                "jobcategories"
            };
        }

        public static List<string> GetControllerList()
        {
            return new List<string>()
            {
                "home"
            };
        }
    }
}