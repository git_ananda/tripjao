﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TripJao.Models
{
    public class Country
    {
        public long ID { get; set; }

        [DisplayName("Country Name")]
        public string CountryName { get; set; }

        [DisplayName("Country Code")]
        public string CountryCode { get; set; }

        public long RegionId { get; set; }

        [DisplayName("Region")]
        public string SelectedRegion { get; set; }

        TripJaoEntities db = null;

        public Country()
        {
            db = new TripJaoEntities();
        }
        public List<SelectListItem> Get()
        {
            try
            {
                List<SelectListItem> coountryList = new List<SelectListItem>();
                var data = db.TJ_Country.ToList();
                foreach (var item in data)
                {
                    coountryList.Add(new SelectListItem { Text = item.CountryName, Value = item.ID.ToString() });
                }
                return coountryList;
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
                return new List<SelectListItem>();
            }
        }

        public List<Country> GetList()
        {
            try
            {
                List<Country> data = db.TJ_Country.Select(x => new Country
                {
                    ID = x.ID,
                    CountryName = x.CountryName,
                    CountryCode = x.CountryCode,
                    //RegionId=x.RegionId,
                    SelectedRegion = x.TJ_Region != null ? x.TJ_Region.Name : ""
                }).ToList();
                return data;
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
                return new List<Country>();
            }
        }
    }
}