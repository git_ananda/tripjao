﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TripJao.Models
{
    public class CustomerFeedback
    {
        public long Id { get; set; }

        public String CustomerName { get; set; }

        public String CustomerEmail { get; set; }

        public string FeedbackSubject { get; set; }

        public String CustomerContact { get; set; }
                
        public String CustomerMessage { get; set; }
    }
}