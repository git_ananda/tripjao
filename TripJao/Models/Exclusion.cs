﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TripJao.Models
{
    public class Exclusion
    {
        public long ID { get; set; }
        [DisplayName("Exclusion Name")]
        public string ExclusionName { get; set; }
        [DisplayName("Exclusion Icon Image")]
        public string ExclusionIconPath { get; set; }
        public string ThumbnailPath { get; private set; }

        TripJaoEntities db = null;
        public Exclusion()
        {
            db = new TripJaoEntities();
        }
        public List<SelectListItem> Get()
        {
            try
            {
                List<SelectListItem> exclusions = new List<SelectListItem>();
                var exclusionList = db.TJ_ExclusionMaster.Select(x => new { x.ID, x.ExclusionName }).ToList();
                foreach (var item in exclusionList)
                {
                    exclusions.Add(new SelectListItem { Text = item.ExclusionName, Value = item.ID.ToString() });
                }
                return exclusions;
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
                return new List<SelectListItem>();
            }
        }

        public List<Exclusion> GetList()
        {
            try
            {
                List<Exclusion> data = db.TJ_ExclusionMaster.AsEnumerable().Select(x => new Exclusion
                {
                    ID = x.ID,
                    ExclusionName = x.ExclusionName,
                    ExclusionIconPath = x.ExclusionIconPath,
                    ThumbnailPath = GetThumbnailPath(x.ExclusionName)
                }).ToList();
                return data;
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
                return new List<Exclusion>();
            }
        }

        public string GetThumbnailPath(string ProductIternaryCode)
        {
            try
            {
                string path = System.Web.Hosting.HostingEnvironment.MapPath("/Uploads/Exclusions/" + ExclusionName + "/Thumbnail/");
                if (Directory.Exists(path))
                {
                    string[] fileEntries = new DirectoryInfo(path).GetFiles().Select(o => o.Name).ToArray();
                    if (fileEntries.Count() > 0)
                    {
                        path = "/Uploads/Exclusions/" + ExclusionName + "/Thumbnail/" + fileEntries[0];
                    }
                    else
                    {
                        path = "/Content/Images/default_tour.jpg";
                    }
                }
                else
                {
                    path = "/Content/Images/default_tour.jpg";
                }
                return path;
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
                return "";
            }
        }

        public List<CheckBoxes> GetCheckBoxList()
        {
            try
            {
                List<CheckBoxes> exclusions = new List<CheckBoxes>();
                var exclusionList = db.TJ_ExclusionMaster.Select(x => new { x.ID, x.ExclusionName }).ToList();
                foreach (var item in exclusionList)
                {
                    exclusions.Add(new CheckBoxes { Text = item.ExclusionName, Checked = item.ID == 1 ? true : false, Id = item.ID});
                }
                return exclusions;
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
                return new List<CheckBoxes>();
            }
        }
    }
}