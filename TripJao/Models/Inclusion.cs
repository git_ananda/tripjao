﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TripJao.Models
{
    public class Inclusion
    {
        public long ID { get; set; }
        [DisplayName("Inclusion Name")]
        public string InclusionName { get; set; }
        [DisplayName("Inclusion Icon Image")]
        public string InclusionIconPath { get; set; }
        public string ThumbnailPath { get; private set; }

        TripJaoEntities db = null;
        public Inclusion()
        {
            db = new TripJaoEntities();
        }
        public List<SelectListItem> Get()
        {
            try
            {
                List<SelectListItem> inclusions = new List<SelectListItem>();
                var inclusionList = db.TJ_InclusionMaster.Select(x => new { x.ID, x.InclusionName }).ToList();
                foreach (var item in inclusionList)
                {
                    inclusions.Add(new SelectListItem { Text = item.InclusionName, Value = item.ID.ToString() });
                }
                return inclusions;
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
                return new List<SelectListItem>();
            }
        }

        public List<Inclusion> GetList()
        {
            try
            {
                List<Inclusion> data = db.TJ_InclusionMaster.AsEnumerable().Select(x => new Inclusion
                {
                    ID = x.ID,
                    InclusionName = x.InclusionName,
                    InclusionIconPath = x.InclusionIconPath,
                    ThumbnailPath = GetThumbnailPath(x.InclusionName)
                }).ToList();
                return data;
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
                return new List<Inclusion>();
            }
        }

        public string GetThumbnailPath(string ProductIternaryCode)
        {
            try
            {
                string path = System.Web.Hosting.HostingEnvironment.MapPath("/Uploads/Inclusions/" + InclusionName + "/Thumbnail/");
                if (Directory.Exists(path))
                {
                    string[] fileEntries = new DirectoryInfo(path).GetFiles().Select(o => o.Name).ToArray();
                    if (fileEntries.Count() > 0)
                    {
                        path = "/Uploads/Inclusions/" + InclusionName + "/Thumbnail/" + fileEntries[0];
                    }
                    else
                    {
                        path = "/Content/Images/default_tour.jpg";
                    }
                }
                else
                {
                    path = "/Content/Images/default_tour.jpg";
                }
                return path;
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
                return "";
            }
        }

        public List<CheckBoxes> GetCheckBoxList()
        {
            try
            {
                List<CheckBoxes> inclusions = new List<CheckBoxes>();
                var inclusionList = db.TJ_InclusionMaster.Select(x => new { x.ID, x.InclusionName }).ToList();
                foreach (var item in inclusionList)
                {
                    inclusions.Add(new CheckBoxes { Text = item.InclusionName, Checked = item.ID == 1 ? true : false, Id = item.ID});
                }
                return inclusions;
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
                return new List<CheckBoxes>();
            }
        }
    }
}