﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TripJao.Models;

namespace TripJao.Models
{
    public class Iternaries
    {
        TripJaoEntities db = null;
        public Iternaries()
        {
            db = new TripJaoEntities();
        }

        public long Id { get; set; }

        [DisplayName("Iternary Code")]
        public string IternaryCode { get; set; }

        [DisplayName("Region")]
        public long RegionId { get; set; }
        public string SelectedRegion { get; set; }

        [DisplayName("Category")]
        public long CategoryId { get; set; }
        public string SelectedCategory { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }

        [DisplayName("Status")]
        public bool Status { get; set; }
        public string SelectedStatus { get; set; }

        public static List<SelectListItem> GetStatus()
        {
            return new List<SelectListItem>
            {
                new SelectListItem { Text = "Online", Value = "true"},
                new SelectListItem { Text = "Offline", Value = "false"}
            };
        }

        public class ItenaryStatusValue
        {
            public string Name { get; set; }
            public string Id { get; set; }
            public static string Online { get { return "1"; } }
            public static string Offline { get { return "0"; } }
            public static string New { get { return "2"; } }
            public static string Unique { get { return ""; } }
            public static List<SelectListItem> GetItenaryStatus()
            {
                return new List<SelectListItem>
                {
                    new SelectListItem { Text = "New", Value = New},
                    new SelectListItem { Text = "Online", Value = Online},
                    new SelectListItem { Text = "Offline", Value = Offline}
                };
            }
            public static List<ItenaryStatusValue> GetList()
            {
                List<ItenaryStatusValue> list = new List<ItenaryStatusValue>
                {
                    new ItenaryStatusValue {Id = Online, Name = "Online"  },
                    new ItenaryStatusValue {Id = Offline, Name = "Offline" },
                    new ItenaryStatusValue {Id = New, Name = "New" },
                    new ItenaryStatusValue {Id = Unique, Name = "Unique"},
                };
                return list;
            }
        }
        public List<Iternaries> Get()
        {
            try
            {
                List<Iternaries> ItenaryList = db.TJ_Iternary.Select(x => new Iternaries
                {
                    SelectedCategory = x.TJ_Category != null ? x.TJ_Category.Name : "",
                    CategoryId = x.CategoryId,
                    Description = x.Description,
                    IternaryCode = x.IternaryCode,
                    Id = x.Id,
                    RegionId = x.RegionId,
                    SelectedRegion = x.TJ_Region != null ? x.TJ_Region.Name : "",
                    Status = x.Status
                }).ToList();
                return ItenaryList;
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
                return new List<Iternaries>();
            }
        }
        public List<SelectListItem> SelectList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            foreach (var item in Get().ToList())
            {
                list.Add(new SelectListItem { Text = item.IternaryCode, Value = item.Id.ToString() });
            }
            return list;
        }
    }
}