﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TripJao.Models
{
    public class JobCategory
    {
        public long Id { get; set; }

        [DisplayName("Job Category")]
        [Required(ErrorMessage = "Job Category is Required", AllowEmptyStrings = false)]
        [RegularExpression(@"^[a-zA-Z0-9 \- &]+$",
                            ErrorMessage = "Job Category is not valid")]
        public string JobCategoryName { get; set; }
        TripJaoEntities db = null;

        public JobCategory()
        {
            db = new TripJaoEntities();
        }

        public List<SelectListItem> Get()
        {
            try
            {
                List<SelectListItem> jobCategory = new List<SelectListItem>();
                var data = db.TJ_Job_Category.Select(x => new { x.ID, x.Job_Cat_Name }).ToList();
                foreach (var item in data)
                {
                    jobCategory.Add(new SelectListItem { Text = item.Job_Cat_Name, Value = item.ID.ToString() });
                }
                return jobCategory;
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
                return new List<SelectListItem>();
            }
        }

        public List<JobCategory> GetList()
        {
            try
            {
                List<JobCategory> data = db.TJ_Job_Category.Select(x => new JobCategory
                {
                    Id = x.ID,
                    JobCategoryName = x.Job_Cat_Name
                }).ToList();
                return data;
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
                return new List<JobCategory>();
            }
        }
    }
}