﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TripJao.Models
{
    public class JobVacancies
    {
        public long Id { get; set; }

        [DisplayName("Job Code")]
        [Required(ErrorMessage = "Job Code is Required", AllowEmptyStrings = false)]
        [RegularExpression(@"^[a-zA-Z0-9 \- &]+$",
                            ErrorMessage = "Job Code is not valid")]
        public string JobCode { get; set; }


        [DisplayName("Job Title")]
        [Required(ErrorMessage = "Job Title is Required", AllowEmptyStrings = false)]
        [RegularExpression(@"^[a-zA-Z. \- &][a-zA-Z. \- &]+$",
                            ErrorMessage = "Job Title is not valid")]
        public string JobTitle { get; set; }

        public long JobCategoryId { get; set; }

        [DisplayName("Job Category")]
        public string SelectedJobCategory { get; set; }


        [DisplayName("Designation")]
        [Required(ErrorMessage = "Job Designation is Required", AllowEmptyStrings = false)]
        [RegularExpression(@"^[a-zA-Z. \- &][a-zA-Z. \- &]+$",
                            ErrorMessage = "Job Designation is not valid")]
        public string JobDesignation { get; set; }

        [DisplayName("Qualification")]
        [Required(ErrorMessage = "Job Qualification is Required", AllowEmptyStrings = false)]
        [RegularExpression(@"^[a-zA-Z. \- &][a-zA-Z., \- &]+$",
                            ErrorMessage = "Job Qualification is not valid")]
        public string Qualification { get; set; }

        [DisplayName("Min. Experience")]
        [Required(ErrorMessage = "Minimum Experience is Required", AllowEmptyStrings = false)]
        [RegularExpression(@"^[0-9. &][0-9. &]+$",
                            ErrorMessage = "Minimum Experience is not valid")]
        public string MinExperience { get; set; }

        [DisplayName("Max. Experience")]
        [Required(ErrorMessage = "Maximum Experience is Required", AllowEmptyStrings = false)]
        [RegularExpression(@"^[0-9. &][0-9. &]+$",
                            ErrorMessage = "Maximum Experience is not valid")]
        public string MaxExperience { get; set; }

        [DisplayName("CTC")]
        [RegularExpression(@"^[0-9. &][0-9.&]+$",
                            ErrorMessage = "Job CTC is not valid")]
        public Nullable<decimal> JobCTC { get; set; }

        [DisplayName("Job Description")]
        [Required(ErrorMessage = "Job Description is Required", AllowEmptyStrings = false)]
        public string JobDescription { get; set; }

        TripJaoEntities db = null;
        public JobVacancies()
        {
            db = new TripJaoEntities();
        }

        //public List<SelectListItem> Get()
        //{
        //    try
        //    {
        //        List<SelectListItem> careers = new List<SelectListItem>();
        //        var data = db.TJ_JobVacancies.Select(x => new { x.Id, x.JobCode, x.JobTitle, x.JobDesignation, x.JobDescription, x.JobCTC, x.MaxExperience, x.MinExperience }).ToList();
        //        foreach (var item in data)
        //        {
        //            careers.Add(new SelectListItem { Id= item.Id.ToString(), JobCode = item.Name });
        //        }
        //        return careers;
        //    }
        //    catch (Exception ex)
        //    {
        //        string errMsg = ex.Message;
        //        return new List<SelectListItem>();
        //    }
        //}

        public List<JobVacancies> GetList()
        {
            try
            {
                List<JobVacancies> data = db.TJ_JobVacancies.Select(x => new JobVacancies
                {
                    Id = x.Id,
                    JobCode=x.JobCode,
                    JobTitle = x.JobTitle,
                    SelectedJobCategory = x.TJ_Job_Category != null ? x.TJ_Job_Category.Job_Cat_Name : "",
                    JobDesignation =x.JobDesignation,
                    Qualification=x.JobQualification,
                    JobDescription=x.JobDescription,
                    JobCTC=x.JobCTC,
                    MaxExperience=x.MaxExperience,
                    MinExperience=x.MinExperience
                }).ToList();
                return data;
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
                return new List<JobVacancies>();
            }
        }
    }
}