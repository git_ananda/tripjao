﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TripJao.Models
{
    public class Package
    {
        public  int? Page { get; set; }
        public string Category { get; set; }
        public long? CategoryId { get; set; }
        public string Duration { get; set; }
        public long? DurationId { get; set; }
        public string Destination { get; set; }
        //public List<Products> Products { get; set; }
        public List<SelectListItem> Categories { get; set; }
        public List<SelectListItem> Durations { get; set; }
    }
}