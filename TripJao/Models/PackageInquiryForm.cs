﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TripJao.Models
{
    public class PackageEnquiryForm
    {
        public long Id { get; set; }

        public String Name { get; set; }

        public String Email { get; set; }

        public string ContactNumber { get; set; }

        public int NoOfTravellers { get; set; }

        public DateTime TentativeDate { get; set; }

        public String Enquiry { get; set; }

        public string Address { get; set; }

        public string Destination { get; set; }

        public string TourType { get; set; }

        public long? ProductId { get; set; }

        public string Department { get; set; }

        public string InstituteName { get; set; }

        public decimal TentativeBudget { get; set; }

        public Nullable<int> Duration { get; set; }

        public Nullable<int> AdultCount { get; set; }

        public Nullable<int> ChildCount { get; set; }

        public Nullable<int> NoOfStudents { get; set; }

        public Nullable<int> NoOfTeachers { get; set; }        

        public Products SelectedProduct { get; set; }

    }

    
}