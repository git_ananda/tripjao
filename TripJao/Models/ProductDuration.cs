﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TripJao.Models
{
    public class ProductDuration
    {
        public static List<SelectListItem> GetSelectList()
        {
            try
            {
                List<SelectListItem> categories = new List<SelectListItem>
                {
                    new SelectListItem{Text = "1 Day Tour", Value = "1"},
                    new SelectListItem{Text = "2-4 Days Tour", Value = "2"},
                    new SelectListItem{Text = "5-7 Days Tour", Value = "5"},
                    new SelectListItem{Text = "7+ Days Tour", Value = "7"},
                };
                return categories;
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
                return new List<SelectListItem>();
            }
        }
    }
}