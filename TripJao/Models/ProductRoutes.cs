﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TripJao.Models;

namespace TripJao.Models
{
    public class ProductRoutes
    {
        public long Id { get; set; }
        public long ProductId { get; set; }

        [DisplayName("Product")]
        public string SelectedProduct { get; set; }

        [DisplayName("City")]
        public string City { get; set; }

        [DisplayName("Pickup Point")]
        public string PickupPoint { get; set; }

        [DisplayName("Drop Point")]
        public string DropPoint { get; set; }

        [AllowHtml]
        [UIHint("tinymce_full")]
        [DisplayName("Description")]
        public string Description { get; set; }

        public int TotalDays { get; set; }
        public int? Day { get; set; }

        TripJaoEntities db = null;
        public ProductRoutes()
        {
            db = new TripJaoEntities();
        }
        public List<SelectListItem> Get()
        {
            try
            {
                List<SelectListItem> regions = new List<SelectListItem>();
                var data = db.TJ_Products.Select(x => new { x.Id, x.ProductIternaryCode}).ToList();
                foreach (var item in data)
                {
                    regions.Add(new SelectListItem { Text = item.ProductIternaryCode, Value = item.Id.ToString()});
                }
                return regions;
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
                return new List<SelectListItem>();
            }
        }
        public static int? GetTotalDays(string ProductId)
        {
            try
            {
                TripJaoEntities db = new TripJaoEntities();
                long Id = Convert.ToInt64(ProductId);
                var totalDays = db.TJ_Products.Where(x => x.Id == Id).Select(x => x.NoOfDays).FirstOrDefault();
                return totalDays;
            }
            catch(Exception ex)
            {
                string errMsg = ex.Message;
                return 0;
            }            
        }
        public List<ProductRoutes> GetList()
        {
            try
            {
                //var latestProductId = db.TJ_Product_Routes.Select(x=> x.ProductId).AsEnumerable().LastOrDefault();

                List<ProductRoutes> routes = db.TJ_Product_Routes/*.Where(x => x.ProductId == latestProductId)*/.Select(route => new ProductRoutes
                {
                    SelectedProduct = route.TJ_Products != null ? route.TJ_Products.ProductIternaryCode : null,
                    City = route.City,
                    DropPoint = route.DropPoint,
                    PickupPoint = route.PickupPoint,
                    Id = route.Id,
                    ProductId = route.ProductId
                }).ToList();

                return routes;
            }
            catch(Exception ex)
            {
                string errMsg = ex.Message;
                return new List<ProductRoutes>();
            }
        }
    }
}