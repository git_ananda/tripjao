﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TripJao.Models
{
    public class ProductTypes
    {
        public static string Promotion { get { return "Promotion"; } }
        public static string DomesticTour { get { return "Domestic Tours"; } }
        public static string InternationalTour { get { return "International Tours"; } }
        public static string CorporateTour { get { return "Corporate Tours"; } }
        public static string CulturalTour { get { return "Cultural Tours"; } }
        public static string FarByCar { get { return "Far By Car"; } }
        public static string EducationalTours { get { return "Educational Tours"; } }
        public static string IndustrialTours { get { return "Industrial Visit"; } }
        public static string ExposSummetsTours { get { return "Educational Expos & Summets"; } }
        public static string EducationalCompetition { get { return "Educational Competition"; } }
        public static string EducationaInternational { get { return "Educational International Tours"; } }
        public static string HoneymoonTours { get { return "Honeymoon Tours"; } }
    }
}