﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using TripJao.Models;

namespace TripJao.Models
{
    public class Products
    {
        public long Id { get; set; }

        [DisplayName("Business Type")]
        public string BusinessType { get; set; }

        public long? SeasonId { get; set; }

        [DisplayName("Season")]
        public string SelectedSeason { get; set; }

        [DisplayName("Valid From Date")]
        public DateTime? ValidFrom { get; set; }

        [DisplayName("Valid To Date")]
        public DateTime? ValidTo { get; set; }

        public long? IternaryId { get; set; }

        [DisplayName("Iternary")]
        public string SelectedIternary { get; set; }

        [DisplayName("Description")]
        public string IternaryDescription { get; set; }

        [DisplayName("Category")]
        public string IternaryCategory { get; set; }

        public long IternaryCategoryId { get; set; }

        [DisplayName("Product Code")]
        public string ProductIternaryCode { get; set; }

        [DisplayName("Total Days")]
        public int? NoOfDays { get; set; }

        [DisplayName("Number Of Nights")]
        public int? NoOfNights { get; set; }

        [DisplayName("Days and Nights")]
        public string DaysAndNights { get; set; }

        [DisplayName("Status")]
        public string IternaryStatus { get; set; }

        [DisplayName("Remark")]
        public string Remark { get; set; }

        public Nullable<long> ZoneId { get; set; }

        [DisplayName("Zone")]
        public string SelectedZone { get; set; }

        public Nullable<long> CityId { get; set; }

        [DisplayName("City")]
        public string SelectedCity { get; set; }

        public Nullable<long> RegionId { get; set; }

        [DisplayName("Region")]
        public string SelectedRegion { get; set; }

        [DisplayName("Image")]
        public string Thumbnail { get; set; }

        [DisplayName("Location Path")]
        public string LocationPath { get; set; }

        public string Source { get; set; }

        public string Destination { get; set; }

        [DisplayName("Cost Price")]
        public decimal CostPrice { get; set; }

        public decimal? Discount { get; set; }

        public string SelectedProduct { get; set; }

        [DisplayName("Discounted Price")]
        public decimal? DiscountedPrice { get; set; }

        public string ThumbnailPath { get; set; }

        [DisplayName("Categories")]
        public string Category { get; set; }

        public List<CheckBoxes> Categories { get; set; }
        [DisplayName("Inclusions")]
        public List<CheckBoxes> Inclusions { get; set; }
        public string Inclusion { get; set; }

        [DisplayName("Exclusions")]
        public List<CheckBoxes> Exclusions { get; set; }
        public string Exclusion { get; set; }

        [DisplayName("Top Promotion")]
        public string TopPromotion { get; set; }

        public TJ_Product_Prices Price { get; set; }
        public List<TJ_Product_Routes> Routes { get; set; }

        public PackageEnquiryForm PackageInquiryForm { get; set; }
        public CustomerFeedback customerFeedback { get; set; }
        public List<string> GalleyPaths { get; set; }

        public decimal SellingPrice { get; set; }

        public Boolean isEducationalTour { get;set; }
        
        public string ProductName { get; set; }

        public string CountryName { get; set; }

        [DisplayName("Country")]
        public string SelectedCountry { get; set; }

        TripJaoEntities db = null;

        public Products()
        {
            db = new TripJaoEntities();
        }

        public List<Products> GetHoneymoonList(string honeymoonTours, string SubTypeTours, ref string errMsg)
        {
            try
            {
                var productQuery = db.TJ_Products.Where(x => x.IternaryStatus != Iternaries.ItenaryStatusValue.Unique);

                if (!string.IsNullOrEmpty(honeymoonTours))
                {
                    if (honeymoonTours == ProductTypes.HoneymoonTours)
                    {

                        long CategoryId = db.TJ_Category.Where(x => x.Name.Equals(honeymoonTours)).Select(x => x.Id).FirstOrDefault();
                        long subCategoryId = db.TJ_Category.Where(x => x.Name.Equals(SubTypeTours)).Select(x =>  x.Id).FirstOrDefault();
                        var productIds = db.TJ_Product_Categories.Where(x => x.CategoryId == subCategoryId && x.Checked==true).Select(x => x.ProductId);
                        productQuery = from cat in db.TJ_Product_Categories
                                       join product in db.TJ_Products on  cat.ProductId equals product.Id
                                       where (cat.CategoryId == CategoryId && cat.Checked == true) && productIds.Contains(cat.ProductId) && product.IternaryStatus == Iternaries.ItenaryStatusValue.Online
                                       select product;
                    }

                }
                var data = productQuery.AsEnumerable().Select(x => new Products
                {
                    BusinessType = x.BusinessType,
                    CityId = x.CityId,
                    Id = x.Id,
                    IternaryCategory = string.Join(", ", db.TJ_Product_Categories.Where(z => z.ProductId == x.Id).Select(p => p.Name).ToArray()),
                    IternaryDescription = x.GetDescription(),
                    IternaryId = x.IternaryId,
                    SelectedIternary = null,
                    IternaryStatus = Types.StatusValues.GetStatus(x.IternaryStatus),
                    NoOfDays = x.NoOfDays,
                    NoOfNights = x.NoOfNights,
                    ProductIternaryCode = x.ProductIternaryCode,
                    RegionId = x.RegionId,
                    SelectedRegion = x.TJ_Region != null ? x.TJ_Region.Name : "",
                    Remark = x.Remark,
                    SeasonId = x.SeasonId,
                    SelectedSeason = null,
                    ValidFrom = x.ValidFrom,
                    ValidTo = x.ValidTo,
                    ZoneId = x.ZoneId,
                    Source = x.Source,
                    Destination = x.Destination,
                    Thumbnail = x.Thumbnail,
                    CostPrice = x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().CostPrice : 0,
                    //SellingPrice = x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().SellingPrice : 0,
                    Discount = x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().Discount : 0,
                    DiscountedPrice = x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().DiscountedPrice : 0,// x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().DiscountedPrice : 0,
                    //Math.Round(Convert.ToDecimal((x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().SellingPrice : 0) - (((x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().SellingPrice : 0) *
                    //                    (x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().Discount : 0)) / 100)), 2),
                    ThumbnailPath = GetThumbnailPath(x.ProductIternaryCode),
                    Inclusion = string.Join(", ", db.TJ_Product_Inclusions.Where(z => z.ProductId == x.Id).Select(p => p.Name).ToArray()),
                    Exclusion = string.Join(", ", db.TJ_Product_Exclusions.Where(z => z.ProductId == x.Id).Select(p => p.Name).ToArray()),
                    TopPromotion = x.TopPromotion,
                    GalleyPaths = GetGallryPaths(x.ProductIternaryCode),
                    ProductName = x.ProductName
                }).OrderByDescending(x => x.Id).ToList();

                return data;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
                return new List<Products>();
            }
        }

        public List<Products> getEducationalTopTours(string educationalTours, ref string errMsg)
        {
            try
            {
                var productQuery = db.TJ_Products.Where(x => x.IternaryStatus != Iternaries.ItenaryStatusValue.Unique);

                if (!string.IsNullOrEmpty(educationalTours))
                {
                    if (educationalTours == ProductTypes.EducationalTours)
                    {

                        long CategoryId = db.TJ_Category.Where(x => x.Name.Equals(educationalTours)).Select(x => x.Id).FirstOrDefault();
                        var EduToursId=db.TJ_Category.Where(x => x.Name.Equals(ProductTypes.EducationaInternational) || x.Name.Equals(ProductTypes.EducationalCompetition)|| x.Name.Equals(ProductTypes.ExposSummetsTours) || x.Name.Equals(ProductTypes.IndustrialTours)).Select(x => x.Id);
                        var productIds = db.TJ_Product_Categories.Where(x => EduToursId.Contains((long)x.CategoryId) && x.Checked == true).Select(x => x.ProductId).Distinct();
                        productQuery = from cat in db.TJ_Product_Categories
                                       join product in db.TJ_Products on cat.ProductId equals product.Id
                                       where (cat.CategoryId == CategoryId && cat.Checked == true) && productIds.Contains(cat.ProductId) && product.IternaryStatus == Iternaries.ItenaryStatusValue.Online
                                       select product;
                    }
                    
                }
                var data = productQuery.AsEnumerable().Select(x => new Products
                {
                    BusinessType = x.BusinessType,
                    CityId = x.CityId,
                    Id = x.Id,
                    IternaryCategory = string.Join(", ", db.TJ_Product_Categories.Where(z => z.ProductId == x.Id).Select(p => p.Name).ToArray()),
                    IternaryDescription = x.GetDescription(),
                    IternaryId = x.IternaryId,
                    SelectedIternary = null,
                    IternaryStatus = Types.StatusValues.GetStatus(x.IternaryStatus),
                    NoOfDays = x.NoOfDays,
                    NoOfNights = x.NoOfNights,
                    ProductIternaryCode = x.ProductIternaryCode,
                    RegionId = x.RegionId,
                    SelectedRegion = x.TJ_Region != null ? x.TJ_Region.Name : "",
                    Remark = x.Remark,
                    SeasonId = x.SeasonId,
                    SelectedSeason = null,
                    ValidFrom = x.ValidFrom,
                    ValidTo = x.ValidTo,
                    ZoneId = x.ZoneId,
                    Source = x.Source,
                    Destination = x.Destination,
                    Thumbnail = x.Thumbnail,
                    CostPrice = x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().CostPrice : 0,
                    //SellingPrice = x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().SellingPrice : 0,
                    Discount = x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().Discount : 0,
                    DiscountedPrice = x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().DiscountedPrice : 0,// x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().DiscountedPrice : 0,
                    //Math.Round(Convert.ToDecimal((x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().SellingPrice : 0) - (((x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().SellingPrice : 0) *
                    //                    (x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().Discount : 0)) / 100)), 2),
                    ThumbnailPath = GetThumbnailPath(x.ProductIternaryCode),
                    Inclusion = string.Join(", ", db.TJ_Product_Inclusions.Where(z => z.ProductId == x.Id).Select(p => p.Name).ToArray()),
                    Exclusion = string.Join(", ", db.TJ_Product_Exclusions.Where(z => z.ProductId == x.Id).Select(p => p.Name).ToArray()),
                    TopPromotion = x.TopPromotion,
                    GalleyPaths = GetGallryPaths(x.ProductIternaryCode),
                    ProductName = x.ProductName
                }).OrderByDescending(x => x.Id).ToList();

                return data;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
                return new List<Products>();
            }
        }
        public List<Products> GetList(string categoryName, ref string errMsg)
        {
            try
            {
                var productQuery = db.TJ_Products.Where(x => x.IternaryStatus != Iternaries.ItenaryStatusValue.Unique);

                if (!string.IsNullOrEmpty(categoryName))
                {
                    if (categoryName != ProductTypes.Promotion)
                    {

                        long CategoryId = db.TJ_Category.Where(x => x.Name.Equals(categoryName)).Select(x => x.Id).FirstOrDefault();

                        productQuery = from cat in db.TJ_Product_Categories
                                       join product in db.TJ_Products on cat.ProductId equals product.Id
                                       where cat.CategoryId == CategoryId && cat.Checked==true && product.IternaryStatus == Iternaries.ItenaryStatusValue.Online
                                       select product;
                    }

                    if (categoryName == ProductTypes.Promotion)
                    {
                        productQuery = productQuery.Where(x => x.TopPromotion == ProductTopPromotionValue.Yes);
                    }
                }
                var data = productQuery.AsEnumerable().Select(x => new Products
                {
                    BusinessType = x.BusinessType,
                    CityId = x.CityId,
                    Id = x.Id,
                    IternaryCategory = string.Join(", ", db.TJ_Product_Categories.Where(z => z.ProductId == x.Id).Select(p => p.Name).ToArray()),
                    IternaryDescription = x.GetDescription(),
                    IternaryId = x.IternaryId,
                    SelectedIternary = null,
                    IternaryStatus = Types.StatusValues.GetStatus(x.IternaryStatus),
                    NoOfDays = x.NoOfDays,
                    NoOfNights = x.NoOfNights,
                    ProductIternaryCode = x.ProductIternaryCode,
                    RegionId = x.RegionId,
                    SelectedRegion = x.TJ_Region != null ? x.TJ_Region.Name : "",
                    SelectedCountry = x.TJ_Country != null ? x.TJ_Country.CountryName : "",
                    Remark = x.Remark,
                    SeasonId = x.SeasonId,
                    SelectedSeason = null,
                    ValidFrom = x.ValidFrom,
                    ValidTo = x.ValidTo,
                    ZoneId = x.ZoneId,
                    Source = x.Source,
                    Destination = x.Destination,
                    Thumbnail = x.Thumbnail,
                    CostPrice = x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().CostPrice : 0,
                    //SellingPrice = x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().SellingPrice : 0,
                    Discount = x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().Discount : 0,
                    DiscountedPrice = x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().DiscountedPrice : 0,// x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().DiscountedPrice : 0,
                    //Math.Round(Convert.ToDecimal((x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().SellingPrice : 0) - (((x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().SellingPrice : 0) *
                    //                    (x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().Discount : 0)) / 100)), 2),
                    ThumbnailPath = GetThumbnailPath(x.ProductIternaryCode),
                    Inclusion = string.Join(", ", db.TJ_Product_Inclusions.Where(z => z.ProductId == x.Id).Select(p => p.Name).ToArray()),
                    Exclusion = string.Join(", ", db.TJ_Product_Exclusions.Where(z => z.ProductId == x.Id).Select(p => p.Name).ToArray()),
                    TopPromotion = x.TopPromotion,
                    GalleyPaths = GetGallryPaths(x.ProductIternaryCode),
                    ProductName = x.ProductName
                }).OrderByDescending(x => x.Id).ToList();
                return data;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
                return new List<Products>();
            }
        }

        public string GetThumbnailPath(string ProductIternaryCode)
        {
            try
            {
                string path = System.Web.Hosting.HostingEnvironment.MapPath("/Uploads/Packages/" + ProductIternaryCode + "/Thumbnail/");
                if (Directory.Exists(path))
                {
                    string[] fileEntries = new DirectoryInfo(path).GetFiles().Select(o => o.Name).ToArray();
                    if (fileEntries.Count() > 0)
                    {
                        path = "/Uploads/Packages/" + ProductIternaryCode + "/Thumbnail/" + fileEntries[0];
                    }
                    else
                    {
                        path = "/Content/Images/default_tour.jpg";
                    }
                }
                else
                {
                    path = "/Content/Images/default_tour.jpg";
                }
                return path;
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
                return "";
            }
        }

        public Products GetDetails(long Id)
        {
            try
            {
                var data = db.TJ_Products.Where(x => x.Id == Id).AsEnumerable().Select(x => new Products
                {
                    BusinessType = x.BusinessType,
                    CityId = x.CityId,
                    Id = x.Id,
                    IternaryCategory = string.Join(", ", db.TJ_Product_Categories.Where(z => z.ProductId == x.Id && z.Checked == true).Select(p => p.Name).ToArray()),
                    IternaryDescription = x.IternaryDescription,
                    IternaryId = x.IternaryId,
                    SelectedIternary = null,
                    IternaryStatus = Types.StatusValues.GetStatus(x.IternaryStatus),
                    NoOfDays = x.NoOfDays,
                    NoOfNights = x.NoOfNights,
                    ProductIternaryCode = x.ProductIternaryCode,
                    RegionId = x.RegionId,
                    SelectedRegion = x.TJ_Region != null ? x.TJ_Region.Name : "",
                    Remark = x.Remark,
                    SeasonId = x.SeasonId,
                    SelectedSeason = null,
                    ValidFrom = x.ValidFrom,
                    ValidTo = x.ValidTo,
                    ZoneId = x.ZoneId,
                    Source = x.Source,
                    Destination = x.Destination,
                    Thumbnail = x.Thumbnail,
                    CostPrice = x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().CostPrice : 0,
                    //SellingPrice = x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().SellingPrice : 0,
                    Discount = x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().Discount : 0,
                    DiscountedPrice = x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().DiscountedPrice : 0,
                    //Math.Round(Convert.ToDecimal((x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().SellingPrice : 0) - (((x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().SellingPrice : 0) *
                    //                    (x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().Discount : 0)) / 100)), 2),
                    ThumbnailPath = GetThumbnailPath(x.ProductIternaryCode),
                    Inclusion = string.Join(", ", db.TJ_Product_Inclusions.Where(z => z.ProductId == x.Id).Select(p => p.Name).ToArray()),
                    Exclusion = string.Join(", ", db.TJ_Product_Exclusions.Where(z => z.ProductId == x.Id).Select(p => p.Name).ToArray()),
                    TopPromotion = x.TopPromotion,
                    LocationPath = x.LocationPath ?? "https://www.google.co.in/maps/place/Trip+Jao/@18.506462,73.8252193,17z/data=!3m1!4b1!4m5!3m4!1s0x3bc2bf92fcd97bef:0xa3bcb06bb210bd18!8m2!3d18.506462!4d73.827408",
                    Categories = db.TJ_Product_Categories.Where(c => c.ProductId == x.Id).AsEnumerable().Select(cat => new CheckBoxes
                    {
                        Text = cat.Name
                    }).ToList(),
                    Inclusions = db.TJ_Product_Inclusions.Where(c => c.ProductId == x.Id).AsEnumerable().Select(cat => new CheckBoxes
                    {
                        Text = cat.Name
                    }).ToList(),
                    Exclusions = db.TJ_Product_Exclusions.Where(c => c.ProductId == x.Id).AsEnumerable().Select(cat => new CheckBoxes
                    {
                        Text = cat.Name
                    }).ToList(),
                    Price = db.TJ_Product_Prices.Where(p => p.ProductId == x.Id).FirstOrDefault(),
                    Routes = db.TJ_Product_Routes.Where(r => r.ProductId == x.Id).OrderBy(d => d.Day).ToList(),
                    GalleyPaths = GetGallryPaths(x.ProductIternaryCode),
                    ProductName = x.ProductName,
                    CountryName = x.TJ_Country != null ? x.TJ_Country.CountryName : ""
                }).FirstOrDefault();

                return data;
            }
            catch (Exception ex)
            {
                string erMsg = ex.Message;
                return null;
            }
        }

        
        public List<string> GetGallryPaths(string productCode)
        {
            try
            {
                List<string> data = new List<string>();
                string path = System.Web.Hosting.HostingEnvironment.MapPath("/Uploads/Packages/" + productCode + "/Gallary/");
                if (Directory.Exists(path))
                {
                    string[] fileEntries = new DirectoryInfo(path).GetFiles().Select(o => o.Name).ToArray();
                    if (fileEntries.Count() > 0)
                    {
                        foreach (var item in fileEntries)
                        {
                            path = "/Uploads/Packages/" + productCode + "/Gallary/" + item;
                            data.Add(path);
                        }
                    }
                    else
                    {
                        path = "/Content/Images/default_tour.jpg";
                        data.Add(path);
                    }
                }
                else
                {
                    path = "/Content/Images/default_tour.jpg";
                    data.Add(path);
                }

                return data;
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
                return new List<string>();
            }
        }

        public static string Delete(long Id)
        {
            TripJaoEntities db = new TripJaoEntities();
            var package = db.TJ_Products.Where(x => x.Id == Id).FirstOrDefault();
            if (package != null)
            {
                package.TJ_Product_Categories.ToList().ForEach(x => db.TJ_Product_Categories.Remove(x));
                package.TJ_Product_Category.ToList().ForEach(x => db.TJ_Product_Category.Remove(x));
                package.TJ_Product_Enquiry.ToList().ForEach(x => db.TJ_Product_Enquiry.Remove(x));
                package.TJ_Product_Exclusions.ToList().ForEach(x => db.TJ_Product_Exclusions.Remove(x));
                package.TJ_Product_Inclusions.ToList().ForEach(x => db.TJ_Product_Inclusions.Remove(x));
                package.TJ_Product_Prices.ToList().ForEach(x => db.TJ_Product_Prices.Remove(x));
                package.TJ_Product_Routes.ToList().ForEach(x => db.TJ_Product_Routes.Remove(x));
                package.TJ_Product_Tours.ToList().ForEach(x => db.TJ_Product_Tours.Remove(x));
                package.TJ_PhotoGallary.ToList().ForEach(x => db.TJ_PhotoGallary.Remove(x));
                db.TJ_Products.Remove(package);
                db.SaveChanges();
                return "Package deleted successfully...";
            }
            else
            {
                return "Invalid Package!!!";
            }
        }

        public List<Products> GetAllProductWiseGalleryPaths()
        {
            Products products = new Products();
            List<Products> productWiseGallery = new List<Products>();
            try
            {
                
                var productQuery = db.TJ_Products.Where(x => x.IternaryStatus != Iternaries.ItenaryStatusValue.Unique);
                var data = productQuery.AsEnumerable().Select(x => new Products
                {
                    BusinessType = x.BusinessType,
                    CityId = x.CityId,
                    Id = x.Id,
                    IternaryCategory = string.Join(", ", db.TJ_Product_Categories.Where(z => z.ProductId == x.Id).Select(p => p.Name).ToArray()),
                    IternaryDescription = x.GetDescription(),
                    IternaryId = x.IternaryId,
                    SelectedIternary = null,
                    IternaryStatus = Types.StatusValues.GetStatus(x.IternaryStatus),
                    NoOfDays = x.NoOfDays,
                    NoOfNights = x.NoOfNights,
                    ProductIternaryCode = x.ProductIternaryCode,
                    RegionId = x.RegionId,
                    SelectedRegion = x.TJ_Region != null ? x.TJ_Region.Name : "",
                    Remark = x.Remark,
                    SeasonId = x.SeasonId,
                    SelectedSeason = null,
                    ValidFrom = x.ValidFrom,
                    ValidTo = x.ValidTo,
                    ZoneId = x.ZoneId,
                    Source = x.Source,
                    Destination = x.Destination,
                    Thumbnail = x.Thumbnail,
                    CostPrice = x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().CostPrice : 0,
                    //SellingPrice = x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().SellingPrice : 0,
                    Discount = x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().Discount : 0,
                    DiscountedPrice = x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().DiscountedPrice : 0,// x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().DiscountedPrice : 0,
                    //Math.Round(Convert.ToDecimal((x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().SellingPrice : 0) - (((x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().SellingPrice : 0) *
                    //                    (x.TJ_Product_Prices.Count > 0 ? x.TJ_Product_Prices.FirstOrDefault().Discount : 0)) / 100)), 2),
                    ThumbnailPath = GetThumbnailPath(x.ProductIternaryCode),
                    Inclusion = string.Join(", ", db.TJ_Product_Inclusions.Where(z => z.ProductId == x.Id).Select(p => p.Name).ToArray()),
                    Exclusion = string.Join(", ", db.TJ_Product_Exclusions.Where(z => z.ProductId == x.Id).Select(p => p.Name).ToArray()),
                    TopPromotion = x.TopPromotion,
                    GalleyPaths = GetGallryPaths(x.ProductIternaryCode)
                }).OrderByDescending(x => x.Id).ToList();
                productWiseGallery = data;
                
            }
            catch(Exception ex)
            {
                string errMsg = ex.Message;
                return new List<Products>();

            }
            return productWiseGallery;
        }
            
    
    }

    public class ProductTopPromotionValue
    {
        public string  Name { get; set; }
        public string Id { get; set; }
        public static string No { get { return "No"; } }
        public static string Yes { get { return "Yes"; } }
        public static List<SelectListItem> GetTopPromtionStatus()
        {
            return new List<SelectListItem>
            {
                new SelectListItem { Text = No, Value = No},
                new SelectListItem { Text = Yes, Value = Yes}
            };

        }
        public static List<ProductTopPromotionValue> GetList()
        {
            List<ProductTopPromotionValue> topProm = new List<ProductTopPromotionValue>
            {
                new ProductTopPromotionValue{Name = Yes, Id = Yes},
                new ProductTopPromotionValue{Name = No, Id = No}
            };
            return topProm;
        }
    }

    public partial class TJ_Products
    {
        public string GetDescription()
        {
            string desc = (!String.IsNullOrWhiteSpace(this.IternaryDescription) && this.IternaryDescription.Length >= 50) 
                            ? Regex.Replace(Regex.Match(this.IternaryDescription.ToString(), @"^(\w+\b.*?){13}").ToString(), @"\t|\n|\r", " ")
                            : this.IternaryDescription;

            return desc;
               
        }
        public List<CheckBoxes> Inclusions { get; set; }
        public List<CheckBoxes> Exclusions { get; set; }
        public List<CheckBoxes> Categories { get; set; }
        public List<ProductRoutes> Iternaries { get; set; }
        public TJ_Product_Prices Price { get; set; }
        public string ViewName { get; set; }
    }

    
      

}