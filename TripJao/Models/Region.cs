﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TripJao.Models;

namespace TripJao.Models
{
    public class Region
    {
        public long Id { get; set; }
        [DisplayName("Region Name")]
        [Required(ErrorMessage = "Region Name is Required", AllowEmptyStrings = false)]
        [RegularExpression(@"^[a-zA-Z &][a-zA-Z &]+$",
                            ErrorMessage = "Region Name is not valid")]
        public string Name { get; set; }
        public string Code { get; set; }
        TripJaoEntities db = null;
        public Region()
        {
            db = new TripJaoEntities();
        }
        public List<SelectListItem> Get()
        {
            try
            {
                List<SelectListItem> regions = new List<SelectListItem>();
                var data = db.TJ_Region.Select(x => new { x.Id, x.Name }).ToList();
                foreach (var item in data)
                {
                    regions.Add(new SelectListItem { Text = item.Name, Value = item.Id.ToString() });
                }
                return regions;
            }
            catch(Exception ex)
            {
                string errMsg = ex.Message;
                return new List<SelectListItem>();
            }
        }

        public List<Region> GetList()
        {
            try
            {
                List<Region> data = db.TJ_Region.Select(x => new Region
                {
                    Id = x.Id,
                    Name = x.Name,
                    Code = x.Code
                }).ToList();
                return data;
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
                return new List<Region>();
            }
        }
    }
}