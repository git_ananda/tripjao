﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TripJao.Models;

namespace TripJao.Models
{
    public class Seasons
    {
        TripJaoEntities db = null;
        public Seasons()
        {
            db = new TripJaoEntities();
        }
        public List<SelectListItem> Get()
        {
            try
            {
                List<SelectListItem> list = new List<SelectListItem>();
                var seasonList = db.TJ_Season.ToList();
                foreach(var item in seasonList)
                {
                    list.Add(new SelectListItem
                    {
                        Text = item.Name,
                        Value = item.Id.ToString()
                    });
                }
                return list;
            }
            catch(Exception ex)
            {
                string errMsg = ex.Message;
                return new List<SelectListItem>();
            }
        }
    }
}