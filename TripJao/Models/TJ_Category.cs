//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TripJao.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class TJ_Category
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TJ_Category()
        {
            this.TJ_Iternary = new HashSet<TJ_Iternary>();
            this.TJ_Product_Categories = new HashSet<TJ_Product_Categories>();
            this.TJ_Products = new HashSet<TJ_Products>();
            this.TJ_Product_Category = new HashSet<TJ_Product_Category>();
        }
    
        public long Id { get; set; }
        public string Name { get; set; }
        public string CategoryCode { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TJ_Iternary> TJ_Iternary { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TJ_Product_Categories> TJ_Product_Categories { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TJ_Products> TJ_Products { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TJ_Product_Category> TJ_Product_Category { get; set; }
    }
}
