//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TripJao.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class TJ_ExclusionMaster
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TJ_ExclusionMaster()
        {
            this.TJ_Product_Exclusions = new HashSet<TJ_Product_Exclusions>();
        }
    
        public long ID { get; set; }
        public string ExclusionName { get; set; }
        public string ExclusionIconPath { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TJ_Product_Exclusions> TJ_Product_Exclusions { get; set; }
    }
}
