﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TripJao.Models
{
    public class TourTypes
    {
        public static string FarByCar { get { return "FarByCar"; } }
        public static string Educational { get { return "Educational"; } }
        public static string Package { get { return "Package"; } }
    }
}