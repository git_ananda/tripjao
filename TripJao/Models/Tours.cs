﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TripJao.Models;

namespace TripJao.Models
{
    public class Tours
    {
        TripJaoEntities db = null;
        public string City { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? EndDate { get; set; }
        public Tours()
        {
            db = new TripJaoEntities();
        }       
    }
}