﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TripJao.Models
{
    public class Types
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public static List<SelectListItem> GetBusinessTypes()
        {
            return new List<SelectListItem>
            {
                new SelectListItem{Text = "India", Value = "1"},
                new SelectListItem{Text = "Other", Value = "0"},
            };
        }

        public static List<Types> GetBusinessTypesList()
        {
            List<Types> types = new List<Types>
            {
                new Types {Id = "1", Name = "India"},
                new Types{Id = "2", Name = "Other"}
            };
            return types;
        }

        public static class StatusValues
        {
            public static string New { get { return "2"; } }
            public static string Online { get { return "1"; } }
            public static string Offline { get { return "0"; } }
            public static string GetStatus(string status)
            {
                if (status.Trim() == New)
                    return "New";

                else if (status.Trim() == Online)
                    return "Online";

                else if (status.Trim() == Offline)
                    return "Offline";

                return "";
            }
        }
    }
}