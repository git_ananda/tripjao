﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TripJao.Models
{
    public class Users
    {
        public long Id { get; set; }
        [DisplayName("First Name")]
        public string FirstName { get; set; }
        [DisplayName("Last Name")]
        public string LastName { get; set; }
        [DisplayName("User Type")]
        public string Type { get; set; }

        [DisplayName("User Name")]
        [Required(ErrorMessage = "User Name is Required", AllowEmptyStrings = false)]
        [RegularExpression(@"^([a-zA-Z0-9_\.]+)$",
                            ErrorMessage = "User Name is not valid")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "Password is Required", AllowEmptyStrings = false)]
        [DisplayName("Password")]
        [RegularExpression(@"^([a-zA-Z0-9@$%#_-]+)$",
                            ErrorMessage = "Password is not valid")]
        public string Password { get; set; }
        [DisplayName("Confirm Password")]
        public string ConfirmPassword { get; set; }
        public bool Status { get; set; }
        public Nullable<System.DateTime> createdAt { get; set; }
        public Nullable<System.DateTime> UpdatedAt { get; set; }
        [DisplayName("Remember me")]
        public bool RememberMe { get; set; }
    }
}