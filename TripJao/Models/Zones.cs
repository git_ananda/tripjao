﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TripJao.Models
{
    public class Zones
    {
        public long Id { get; set; }
        public long RegionId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }

        [DisplayName("Region")]
        public string SelectedRegion { get; set; }

        TripJaoEntities db = null;
        
        public Zones() {
            db = new TripJaoEntities();
        }

        public List<SelectListItem> Get()
        {
            try
            {
                List<SelectListItem> zoneList = new List<SelectListItem>();
                var data = db.TJ_Zones.ToList();
                foreach(var item in data)
                {
                    zoneList.Add(new SelectListItem {Text = item.Name, Value = item.Id.ToString() });
                }
                return zoneList;
            }
            catch(Exception ex)
            {
                string errMsg = ex.Message;
                return new List<SelectListItem>();
            }
        }

        public List<Zones> GetList()
        {
            try
            {
                List<Zones> data = db.TJ_Zones.Select(x => new Zones
                {
                    Id = x.Id,
                    Name = x.Name,
                    Code = x.Code,
                    SelectedRegion = x.TJ_Region != null ? x.TJ_Region.Name :""
                }).ToList();
                return data;
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
                return new List<Zones>();
            }
        }

    }
}