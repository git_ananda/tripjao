﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Net;
using System.Runtime.Remoting.Messaging;


namespace TripJao.Utilities
{
    /// <summary>
    ///  This Class is used to send emails.
    /// </summary>

    public sealed class EmailHelper
    {
        string _smtpHost, _smtpUserName, _smtpPassword;

        /// <summary>
        /// Creates a new instance of Common.Email.
        /// </summary>
        public EmailHelper() {
            _smtpHost = System.Web.Configuration.WebConfigurationManager.AppSettings["SMTPServer"].ToString();
            _smtpUserName = System.Web.Configuration.WebConfigurationManager.AppSettings["SMTPUser"].ToString();
            _smtpPassword = System.Web.Configuration.WebConfigurationManager.AppSettings["SMTPPass"].ToString();
        }

        /// <summary>
        /// Creates a new instance of Common.Email.
        /// </summary>
        /// <param name="smtpHost">The host used when performing SMTP transactions.</param>
        /// <param name="smtpUserName">The user name associated with the Smtp host.</param>
        /// <param name="smtpPassword">The password associate with the Smtp user name</param>
        /// 
        public EmailHelper(string smtpHost, string smtpUserName, string smtpPassword)
        {
            _smtpHost = smtpHost;
            _smtpUserName = smtpUserName;
            _smtpPassword = smtpPassword;
        }

        /// <summary>
        /// Sends an email.
        /// </summary>
        /// <param name="subject">The subject of the email.</param>
        /// <param name="contactsToEmail">A collection of people to send the email to.</param>
        /// <param name="fromAddress">The sender of the email.</param>
        /// <param name="body">The body of the email.</param>
        /// <param name="isBodyHtml">A value indicating if the body of the message is Html.</param>
        public void Send(string subject, MailAddressCollection contactsToEmail, string fromAddress, string body, bool isBodyHtml)
        {
            MailMessage mailMessage = null;
            SmtpClient smtpClient = new SmtpClient();
            try
            {
                SetupMailMessage(out mailMessage, subject, contactsToEmail, fromAddress, body, isBodyHtml);
                SetupSmtpClient(out smtpClient);
                smtpClient.Send(mailMessage);

            }
            catch(Exception ex){
                throw ex;
            }
            finally
            {
                DisposeOfMailMessage(mailMessage);
                DisposeOfSmtpClient(smtpClient);
            }
        }

        /// <summary>
        /// Sends an email with more audit data for communications logging.
        /// </summary>
        /// <param name="subject">The subject of the email.</param>
        /// <param name="contactsToEmail">A collection of people to send the email to.</param>
        /// <param name="fromAddress">The sender of the email.</param>
        /// <param name="body">The body of the email.</param>
        /// <param name="isBodyHtml">A value indicating if the body of the message is Html.</param>
        public void SendWithEnhancedLogging(string subject, MailAddressCollection contactsToEmail, string fromAddress, string body, bool isBodyHtml,
            Guid? parentId, Guid? siteId, int? termId, Guid? weekId)
        {
            MailMessage mailMessage = null;
            SmtpClient smtpClient = null;
            try
            {
                SetupMailMessage(out mailMessage, subject, contactsToEmail, fromAddress, body, isBodyHtml);
                SetupSmtpClient(out smtpClient);
                smtpClient.Send(mailMessage);

            }
            finally
            {
                DisposeOfMailMessage(mailMessage);
                DisposeOfSmtpClient(smtpClient);
            }
        }

        /// <summary>
        /// Sets up the MailMessage that will be sent by the Smtp host.
        /// </summary>
        /// <param name="mailMessage">The MailMessage that will be sent by the Smtp host.</param>
        /// <param name="subject">The subject of the email.</param>
        /// <param name="contactsToEmail">A collection of people to send the email to.</param>
        /// <param name="fromAddress">The sender of the email.</param>
        /// <param name="body">The body of the email.</param>
        /// /// <param name="isBodyHtml">A value indicating if the body of the message is Html.</param>
        private void SetupMailMessage(out MailMessage mailMessage, string subject, MailAddressCollection contactsToEmail, string fromAddress, string body, bool isBodyHtml)
        {
            mailMessage = new MailMessage
            {
                Subject = subject,
                From = new MailAddress(fromAddress),
                Body = body,
                IsBodyHtml = isBodyHtml
            };
            foreach (MailAddress contact in contactsToEmail)
            {
                mailMessage.To.Add(contact);
            }
        }

        /// <summary>
        /// Sets up the MailMessage that will be sent by the Smtp host.
        /// </summary>
        /// <param name="mailMessage">The MailMessage that will be sent by the Smtp host.</param>
        /// <param name="subject">The subject of the email.</param>
        /// <param name="contactsToEmail">A collection of people to send the email to.</param>
        /// <param name="fromAddress">The sender of the email.</param>
        /// <param name="body">The body of the email.</param>
        /// /// <param name="isBodyHtml">A value indicating if the body of the message is Html.</param>
        private void SetupMailMessage(out MailMessage mailMessage, string subject, MailAddress contactToEmail, string fromAddress, string body, bool isBodyHtml)
        {
            mailMessage = new MailMessage
            {
                Subject = subject,
                From = new MailAddress(fromAddress),
                Body = body,
                IsBodyHtml = isBodyHtml
            };
            mailMessage.To.Add(contactToEmail);

        }

        /// <summary>
        /// Sends many emails in loop, one at a time to get around the 100 message per connection limit of our mail server.
        /// </summary>
        /// <param name="subject">The subject of the email.</param>
        /// <param name="contactsToEmail">A collection of people to send the email to.</param>
        /// <param name="fromAddress">The sender of the email.</param>
        /// <param name="body">The body of the email.</param>
        /// <param name="isBodyHtml">A value indicating if the body of the message is Html.</param>
        public void SendAsynchBatch(string subject, MailAddressCollection contactsToEmail, string fromAddress, string body, bool isBodyHtml)
        {

            try
            {

                #region create and populate communicationTracking object
                foreach (MailAddress recipient in contactsToEmail)
                {

                    MailMessage mailMessage = new MailMessage();
                    SmtpClient smtpClient = null;
                    SetupMailMessage(out mailMessage, subject, recipient, fromAddress, body, isBodyHtml);
                    SetupSmtpClient(out smtpClient);
                    try
                    {
                        SendThat(mailMessage);
                    }
                    catch (Exception)
                    {
                        //object cleanup then resume next
                        DisposeOfMailMessage(mailMessage);
                        DisposeOfSmtpClient(smtpClient);
                        continue;
                    }

                }

                #endregion

            }
            finally
            {

            }
        }

        /// <summary>
        /// Sets up the SmtpClient that will send emails.
        /// </summary>
        /// <param name="smtpClient">The SmtpClient that will send emails.</param>
        private void SetupSmtpClient(out SmtpClient smtpClient)
        {
            smtpClient = String.IsNullOrEmpty(_smtpHost)
                ? new SmtpClient()
                : new SmtpClient
                {
                    Host = _smtpHost,
                    Credentials = new NetworkCredential(_smtpUserName, _smtpPassword)
                };
            smtpClient.EnableSsl = false;
        }

        /// <summary>
        /// Disposes of a MailMessage.
        /// </summary>
        /// <param name="mailMessage">The MailMessage to be disposed of.</param>
        private void DisposeOfMailMessage(MailMessage mailMessage)
        {
            if (mailMessage != null)
                mailMessage.Dispose();
        }

        /// <summary>
        /// Disposes of an SmtpClient.
        /// </summary>
        /// <param name="smtpClient">The SmtpClient to be disposed of.</param>
        private void DisposeOfSmtpClient(SmtpClient smtpClient)
        {
            if (smtpClient != null)
                smtpClient.Dispose();
        }

        #region Asynchronous emailing
        //based upon code here:
        //http://stackoverflow.com/questions/8747483/proper-way-to-asynchronously-send-an-email-in-asp-net-am-i-doing-it-right

        public void SendThat(MailMessage message)
        {
            AsyncMethodCaller caller = new AsyncMethodCaller(SendMailInSeperateThread);
            AsyncCallback callbackHandler = new AsyncCallback(AsyncCallback);
            caller.BeginInvoke(message, callbackHandler, null);
        }

        private delegate void AsyncMethodCaller(MailMessage message);

        private void SendMailInSeperateThread(MailMessage message)
        {
            SmtpClient client = new SmtpClient();
            try
            {

                client.Timeout = 20000; // 20 second timeout... why more?
                SetupSmtpClient(out client);
                client.Send(message);
                //client.Dispose();
                //message.Dispose();

                // If you have a flag checking to see if an email was sent, set it here
                // Pass more parameters in the delegate if you need to...
            }
            catch (Exception e)
            {
                // This is very necessary to catch errors since we are in
                // a different context & thread
                //Elmah.ErrorLog.GetDefault(null).Log(new Error(e));

                //pl - use our exception logging module
                //ErrorLogHelper.LogException(new ExceptionObject()
                //{
                //    Exception = e,
                //    Project = "KC.Common.EmailHelper",
                //    Module = "EmailHelper.cs",
                //    Method = "SendMailInSeparateThread",
                //    Severity = SeverityType.Low,
                //});
            }
            finally
            {
                client.Dispose();
            }
        }

        private void AsyncCallback(IAsyncResult ar)
        {
            try
            {
                AsyncResult result = (AsyncResult)ar;
                AsyncMethodCaller caller = (AsyncMethodCaller)result.AsyncDelegate;
                caller.EndInvoke(ar);
            }
            catch (Exception e)
            {
                //Elmah.ErrorLog.GetDefault(null).Log(new Error(e));
                //Elmah.ErrorLog.GetDefault(null).Log(new Error(new Exception("Emailer - This hacky asynccallback thing is puking, serves you right.")));
                //pl - use our exception logging module
                //ErrorLogHelper.LogException(new ExceptionObject()
                //{
                //    Exception = e,
                //    Project = "KC.Common.EmailHelper",
                //    Module = "EmailHelper.cs",
                //    Method = "AsyncCallback",
                //    Severity = SeverityType.Low,
                //});
            }
        }

        #endregion //asynch emailing
    }
}
