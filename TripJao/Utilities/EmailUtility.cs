﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using TripJao.Models;

namespace TripJao.Utilities
{
    public class EmailUtility
    {
        public static String sendPackageInquiryMail(PackageEnquiryForm packageInquiry)
        {
            SmtpClient smtpClient = new SmtpClient();
            MailMessage message = new MailMessage();
            String _mailSubject = "";
            String _MailBody = "";

            try
            {
                EmailHelper emailHelper = new EmailHelper();
                MailAddressCollection mailCollection = new MailAddressCollection();
                MailAddress toAddress = new MailAddress("sales@tripjao.com");
                mailCollection.Add(toAddress);
                if (packageInquiry.TourType.Equals(Models.TourTypes.Package)){
                    _mailSubject = "Enquiry for Package " + packageInquiry.SelectedProduct;
                    _MailBody = "<html><head></head><body>Hi TripJao Team !<br><br> Package Enquired : " + packageInquiry.SelectedProduct != null ? (packageInquiry.SelectedProduct.Source + " - " + packageInquiry.SelectedProduct.Destination) : "" +
                                    "<br><br><br> Customer Name : " + packageInquiry.Name +
                                    "<br><br><br> Customer Email : " + packageInquiry.Email +
                                    "<br><br><br> Contact No-: " + packageInquiry.ContactNumber +
                                    "<br><br><br>No. of Travellers : " + packageInquiry.NoOfTravellers +
                                    "<br><br><br> Customer Inquiry : " + packageInquiry.Enquiry;
                }
                else if (packageInquiry.TourType.Equals(Models.TourTypes.Educational))
                {
                    _mailSubject = "Enquiry for Educational Tour ";
                    _MailBody = "<html><head></head><body>Hi TripJao Team !"+
                                    "<br><br><br> Institute Name : " + packageInquiry.InstituteName +
                                    "<br><br><br> Department Name : " + packageInquiry.Department +
                                    "<br><br><br> Faculty Person Name : " + packageInquiry.Name +
                                    "<br><br><br> Email : " + packageInquiry.Email +
                                    "<br><br><br> Contact No-: " + packageInquiry.ContactNumber +
                                    "<br><br><br> Address-: " + packageInquiry.Address +
                                    "<br><br><br> Preffered Destination-: " + packageInquiry.Destination +
                                    "<br><br><br> Tentative Date : " + packageInquiry.TentativeDate +
                                    "<br><br><br> No. of Students : " + packageInquiry.NoOfStudents +
                                    "<br><br><br> No. of Faculty : " + packageInquiry.NoOfTeachers;

                }
                else if (packageInquiry.TourType.Equals(Models.TourTypes.FarByCar))
                {
                    _mailSubject = "Enquiry for Far By Car ";
                    _MailBody = "<html><head></head><body>Hi TripJao Team !" +
                                    "<br><br><br> Customer Name : " + packageInquiry.Name +
                                    "<br><br><br> Email : " + packageInquiry.Email +
                                    "<br><br><br> Contact No-: " + packageInquiry.ContactNumber +
                                    "<br><br><br> Preffered Destination-: " + packageInquiry.Destination +
                                    "<br><br><br> Tentative Budget-: " + packageInquiry.TentativeBudget +
                                    "<br><br><br> Tentative Date : " + packageInquiry.TentativeDate +
                                    "<br><br><br> Duration : " + packageInquiry.Duration +
                                    "<br><br><br> No. of Adults : " + packageInquiry.AdultCount +
                                    "<br><br><br> No. of Children : " + packageInquiry.ChildCount;

                }
                _MailBody = _MailBody + "</body></html>";
                emailHelper.Send(_mailSubject, mailCollection,"contact@tripjao.com",_MailBody,true);
             
                return "Enquiry Submitted successfully !!";
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
                //lblStatus.Text = "Send Email Failed." + ex.Message;
                //this.Page.ErrorPage.Insert(1, ex.Message);
                return errMsg;
            }


        }

        internal static string sendCustomerFeedbackMail(CustomerFeedback customerFeedback)
        {
            String _mailSubject = "";
            String _MailBody = "";

            try
            {
                EmailHelper emailHelper = new EmailHelper();
                MailAddressCollection mailCollection = new MailAddressCollection();
                MailAddress toAddress = new MailAddress("sales@tripjao.com");
                mailCollection.Add(toAddress);
                _mailSubject = "Feedback " + customerFeedback.FeedbackSubject;
                _MailBody = "<html><head></head><body>Hi TripJao ! "+
                                "<br><br><br> Customer Name : " + customerFeedback.CustomerName +
                                "<br><br><br> Customer Email : " + customerFeedback.CustomerEmail +
                                "<br><br><br> Contact No-: " + customerFeedback.CustomerContact +
                                "<br><br><br> Customer Inquiry : " + customerFeedback.CustomerMessage;
                _MailBody = _MailBody + "</body></html>";
                emailHelper.Send(_mailSubject, mailCollection, "contact@tripjao.com", _MailBody, true);

                return "Feedback Submitted successfully !!";
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
                //lblStatus.Text = "Send Email Failed." + ex.Message;
                //this.Page.ErrorPage.Insert(1, ex.Message);
                return errMsg;
            }
        }

        public static String sendReplyToPackageEnquiry(PackageEnquiryForm packageInquiry, String emailbodyFilePath)
        {
            SmtpClient smtpClient = new SmtpClient();
            MailMessage message = new MailMessage();
            String _mailSubject = "";
            String _MailBody = "";

            try
            {
                EmailHelper emailHelper = new EmailHelper();
                MailAddressCollection mailCollection = new MailAddressCollection();
                MailAddress toAddress = new MailAddress(packageInquiry.Email);
                mailCollection.Add(toAddress);
                if (packageInquiry.TourType.Equals(Models.TourTypes.Package)) {
                    _mailSubject = "Enquiry for Package " + packageInquiry.SelectedProduct != null ? packageInquiry.SelectedProduct.Destination : "";
                    using (var reader = new StreamReader(emailbodyFilePath))
                    {
                        _MailBody = reader.ReadToEnd();
                    }
                    _MailBody = _MailBody.Replace("{CustomerName}", packageInquiry.Name);
                    _MailBody = _MailBody.Replace("{PackageTitle}", packageInquiry.SelectedProduct != null ? packageInquiry.SelectedProduct.Source + " - " + @packageInquiry.SelectedProduct.Destination : "");
                    _MailBody = _MailBody.Replace("{PackageThumbnailUrl}", "http://tripjao.com/" + packageInquiry.SelectedProduct.GetThumbnailPath(packageInquiry.SelectedProduct != null ? packageInquiry.SelectedProduct.ProductIternaryCode : ""));
                    _MailBody = _MailBody.Replace("{packageId}", packageInquiry.ProductId != null ? packageInquiry.ProductId.ToString() : "");
                    _MailBody = _MailBody.Replace("{PackageDescription}", Regex.Replace(Regex.Match(packageInquiry.SelectedProduct != null ? packageInquiry.SelectedProduct.IternaryDescription : "", @"^(\w+\b.*?){30}").ToString(), @"\t|\n|\r", " "));
                    //_MailBody = _MailBody.Replace("{Description}", description);
                }
                else if (packageInquiry.TourType.Equals(Models.TourTypes.FarByCar))
                {
                    _mailSubject = "Enquiry for " + Models.TourTypes.FarByCar;
                    using (var reader = new StreamReader(emailbodyFilePath))
                    {
                        _MailBody = reader.ReadToEnd();
                    }
                    _MailBody = _MailBody.Replace("{CustomerName}", packageInquiry.Name);
                    _MailBody = _MailBody.Replace("{PackageTitle}", Models.TourTypes.FarByCar);
                    _MailBody = _MailBody.Replace("{PackageThumbnailUrl}", "http://tripjao.com/Content/Images/farbycar_bg.jpg");
                    _MailBody = _MailBody.Replace("{packageId}", packageInquiry.ProductId != null ? packageInquiry.ProductId.ToString() : "");
                    _MailBody = _MailBody.Replace("{PackageDescription}", Regex.Replace(Regex.Match(packageInquiry.SelectedProduct != null ? packageInquiry.SelectedProduct.IternaryDescription : "", @"^(\w+\b.*?){30}").ToString(), @"\t|\n|\r", " "));
                    //_MailBody = _MailBody.Replace("{Description}", description);
                }
                else if (packageInquiry.TourType.Equals(Models.TourTypes.Educational))
                {
                    _mailSubject = "Enquiry for " + Models.TourTypes.Educational;
                    using (var reader = new StreamReader(emailbodyFilePath))
                    {
                        _MailBody = reader.ReadToEnd();
                    }
                    _MailBody = _MailBody.Replace("{CustomerName}", packageInquiry.Name);
                    _MailBody = _MailBody.Replace("{PackageTitle}", Models.TourTypes.Educational);
                    _MailBody = _MailBody.Replace("{PackageThumbnailUrl}", "http://tripjao.com/Content/Images/Edu_tours_bg.jpg");
                    _MailBody = _MailBody.Replace("{packageId}", packageInquiry.ProductId != null ? packageInquiry.ProductId.ToString() : "");
                    _MailBody = _MailBody.Replace("{PackageDescription}", Regex.Replace(Regex.Match(packageInquiry.SelectedProduct != null ? packageInquiry.SelectedProduct.IternaryDescription : "", @"^(\w+\b.*?){30}").ToString(), @"\t|\n|\r", " "));
                    //_MailBody = _MailBody.Replace("{Description}", description);
                }
                emailHelper.Send(_mailSubject, mailCollection, "contact@tripjao.com", _MailBody, true);

                return "Enquiry Submitted successfully !!";
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
                //lblStatus.Text = "Send Email Failed." + ex.Message;
                //this.Page.ErrorPage.Insert(1, ex.Message);
                return errMsg;
            }


        }
    }
}