﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TripJao.Utilities
{
    public static class PDFHelper
    {
        private static string val(bool value)
        {
            return value ? "yes" : "no";
        }
        public static MvcHtmlString PDFLink(
            this HtmlHelper helper, string linkText, string pdfUrl, int zoom, string target,
            int top, int left, int height, int width, bool status = true, bool toolbar = false,
            bool menubar = false, bool scrollbars = true,
            bool resizable = true, bool titlebar = false)
        {
            var aTag = new TagBuilder("a")
            {
                InnerHtml = linkText
            };
            const string pattern = @"oPdfWindow=window.open('{0}#zoom={1}'),{2},height='{3}', width='{4}', status='{5}',toolbar ='{6}', menubar='{7}', scrollbars='{8}', resizable='{9}', titlebar='{10}', top='{11}', left='{12}'";
            var onclick = String.Format(pattern, pdfUrl, zoom, target, height, width, val(status), val(toolbar), val(menubar), val(scrollbars), val(resizable), val(titlebar), top, left);

            aTag.Attributes.Add("href", "javascript:;");
            aTag.Attributes.Add("onclick", onclick);

            var rawHtml = aTag.ToString();

            return MvcHtmlString.Create(HttpUtility.HtmlDecode(rawHtml));
        }
    }
}